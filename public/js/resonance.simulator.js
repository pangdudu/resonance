function ResonanceSimulator() {
  if (! (this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

  this.init = function() {
    self.image_cache  = {}; // cache for images
    self.dom_cache    = {}; // cache for dom elements
    self.svg          = {}; // storing raphael svg objects in here
    self.svgAttr      = {}; // storing raphael svg object attributes in here
    self.data         = {}; // storing active drawing relevant data in here
    self.mocks        = []; // we use this array to manage our mock clients
    self.settings     = { id:'rsim_'+randomString(8) }; // faye related settings
    self.timers       = { updateDisplayInterval: 5000 }; // timer ids and settings
    // load some default values for svg attributes
    self.loadDefaultSvgAttr();
    self.drawBackground();
    self.drawInterface();
    self.viewDidResize();
    self.setupBayeuxHandlers();
    self.addEventListeners();
  };
  
  this.loadDefaultSvgAttr = function() {
    var a = {};
    a['canvas']     = { width: 1680, height: 1050 };
    a['helperBox']  = { 'stroke': '#f32ccf', 'stroke-width': 4, 'stroke-opacity': 0 };
    a['inputBox']   = { x: 340, y: 360, width: 525, height: 380 };
    a['displayBox'] = { x: 900, y: 130, width: 725, height: 830 };
    a['text']       = { 'text-anchor': 'start', 'font-size': 24
                        ,'fill': '#ffde00', 'font-weight': 'bold' };
    a['headerText'] = { 'font-size': 16, 'fill': '#ffde00', 'font-weight': 'bold' };
    a['mockText']   = { 'font-size': 14, 'fill': '#09b5e3' };
    self.svgAttr = a;  
  };

  this.drawBackground = function() {
    // create the raphael canvas
    self.can = Raphael('canvas', self.svgAttr.canvas.width, self.svgAttr.canvas.height);
    self.can.canvas.setAttribute('viewBox', '0 0 '+self.svgAttr.canvas.width+' '+self.svgAttr.canvas.height);
    self.can.image("/img/resonance_simulator_background.png", 0, 0, self.svgAttr.canvas.width, self.svgAttr.canvas.height);
  };
  
  /* 
   *  Draw interface elements 
   * */
  this.drawInterface = function() {
    // set some sane start values in the input forms
    document.ConfigForm.ClientCardField.value = 1;
    document.ConfigForm.FrequencyField.value  = 10;
    // HELPER BOXES
    var ib = self.svgAttr.inputBox;
    self.svg['inputBox'] = self.can.rect(ib.x,ib.y,ib.width,ib.height,15);
    self.svg.inputBox.attr(self.svgAttr.helperBox);
    var db = self.svgAttr.displayBox;
    self.svg['displayBox'] = self.can.rect(db.x,db.y,db.width,db.height,15);
    self.svg.displayBox.attr(self.svgAttr.helperBox);
    // draw simulator data overview, orientates itself on dimensions of the displayBox
    self.drawDisplay(db.x,db.y,db.width,db.height);
  };


  /* 
   *  Draw simulator information display interface elements 
   * */
  this.drawDisplay = function(x,y,width,height) {
    // draw texts
    self.svg['spawnedClients'] = self.can.text(x + width/6, y + 50, "spawned clients: 0");
    self.svg['msgPer']         = self.can.text(x + width/6, y + 100, "combined msg/s: 0");
    // apply styles
    self.svg.spawnedClients.attr(self.svgAttr.text);
    self.svg.msgPer.attr(self.svgAttr.text);
    
    // draw table header texts
    self.svg.header = self.can.set();
    var hdx = (width - 40)/5;
    self.svg.header.push(self.can.text(x + 20 + hdx*0.5, y + 175, "lifetime(s)"));
    self.svg.header.push(self.can.text(x + 20 + hdx*1.5, y + 175, "id"));
    self.svg.header.push(self.can.text(x + 20 + hdx*2.5, y + 175, "frequency(Hz)"));
    self.svg.header.push(self.can.text(x + 20 + hdx*3.5, y + 175, "run/pause"));
    self.svg.header.push(self.can.text(x + 20 + hdx*4.5, y + 175, "kill"));
    // apply styles
    self.svg.header.attr(self.svgAttr.headerText);
    // draw table container rects
    self.svg.rects = self.can.set();
    var rdx = (width - 30)/5;
    for(var i=0; i < 5; i++){
      self.svg.rects.push(self.can.rect(x + 15 + rdx*i, y + 155, rdx, height-190, 10));  
    };
    // apply style to table container rects
    self.svg.rects.attr({ stroke: '#00e18f' ,'stroke-width': 2 });
  };

  
  /* 
   *  Update simulator information display interface elements 
   * */
  this.updateDisplay = function() {
    // update static texts
    var clientCard = 'spawned clients: '+self.mocks.length;
    self.svg.spawnedClients.attr({ text: clientCard });
    
    // clear prior table content, not pretty, but working fine 
    if(self.svg.mocks){ self.svg.mocks.remove(); };
    self.svg.mocks = self.can.set();
    
    var hdx = (self.svg.displayBox.attrs.width - 40)/5;
    var dbx = self.svg.displayBox.attrs.x + 20;
    var msgs = 0.0;
    
    // iterate over the mock info and display them
    $.each(self.mocks, function(i,mock){
      if(!(mock.attr.pause)){ msgs += parseFloat(mock.attr.freq); };
      // TODO: with these magic numbers the table holds 27 elements
      if(i <= 27){
        // compute y coordinates
        var dy = self.svg.displayBox.attrs.y + 200;
        var my = dy + i * 22;
        // update text
        self.svg.mocks.push(self.can.text(dbx+hdx*0.5, my
            ,(Date.now() - mock.attr.spawnedAt)/1000));
        self.svg.mocks.push(self.can.text(dbx+hdx*1.5, my
            ,mock.attr.id));
        self.svg.mocks.push(self.can.text(dbx+hdx*2.5, my
            ,mock.attr.freq));
        // refresh mock control interfaces
        mock.pauseInterface(self.can, dbx+hdx*3.5, my);
        mock.killInterface(self, dbx+hdx*4.5, my, i);
      };
    });
    
    // apply styles to mock info in the table
    self.svg.mocks.attr(self.svgAttr.mockText);
    // update msg/s text
    self.svg.msgPer.attr({ text: 'combined msg/s: '+msgs });
  }; 

  /*
   *  Resize view
   * */
  this.viewDidResize = function () {
    var width        = self.dom('body').width();
    var windowHeight = $(window).height();
    var canvasHeight = width * (self.svgAttr.canvas.height / self.svgAttr.canvas.width);
    var marginTop    = (windowHeight - canvasHeight) / 2.0;
    // by doing this, we're not in trouble if the canvas was originaly higher than the window
    if(canvasHeight > windowHeight){
      canvasHeight = windowHeight;
      marginTop    = 0;
    };
    self.can.setSize(width, canvasHeight);
    self.dom('#canvas').css({
      'margin-top': marginTop
    });

    // we also need to resize the input elements container
    var canvasWidth = canvasHeight * (self.svgAttr.canvas.width / self.svgAttr.canvas.height);
    var marginLeft = (width - canvasWidth) / 2.0;
    var zoomFactor = (canvasWidth/self.svgAttr.canvas.width);
    self.dom('#simulatorinputs').css({
       'zoom' : zoomFactor
      ,'left' : (self.svgAttr.inputBox.x + marginLeft/zoomFactor)+'px'
      ,'top'  : (self.svgAttr.inputBox.y + marginTop/zoomFactor)+'px'
    });
  };
  
  /* 
   *  The websocket/faye event listeners are setup here 
   * */
  this.setupBayeuxHandlers = function() {
    $.getJSON("/config.json", function (config) {
      self.client = new Faye.Client("http://" + window.location.hostname + ':' + config.port + '/faye', {
        timeout: 120
      });
      // send out a hello!
      self.client.publish('/global/info', { sender:self.settings.id, content:'ResonanceSimulator ready to go!' });
    });
  };

  /* 
   *  Add event listeners and start timers
   * */
  this.addEventListeners = function() {
    $('#btn_start').click(function(){
      $('#btn_start').fadeTo('fast',0.5).fadeTo('fast',1);
      var inputValues = {};
      inputValues['card'] = document.ConfigForm.ClientCardField.value;
      inputValues['freq'] = document.ConfigForm.FrequencyField.value;
      if(inputValues.card && inputValues.freq){
        self.data.input = inputValues;
        // create new mock clients and start simulating data
        if(inputValues.card >= 1 && inputValues.card <= 10){ 
          for(var i=0; i < parseInt(inputValues.card); i++){
            self.mocks.push(new MockClient(inputValues));
            // need a copy of inputValues or all created mocks share it, BAD! 
            inputValues = {};
            inputValues['card'] = document.ConfigForm.ClientCardField.value;
            inputValues['freq'] = document.ConfigForm.FrequencyField.value;
          };
        } else {
          alert('Please choose a client number between 1 and 10.');
        }; 
        // finally trigger an self.updateDisplay()
        self.updateDisplay();
      } else {
        alert('Input incomplete, can not start!');
      }
    });
    // start the timers
    self.timers['updateDisplayTimer'] = setInterval(self.updateDisplay, self.timers.updateDisplayInterval);
  };
  
  /* 
   *  Simple cache for dom elements 
   * */
  this.dom = function(cssId) {
    var domElement;
    if(!(domElement = self.dom_cache[cssId])){
      domElement = $(cssId);
    };
    return domElement;
  };

  this.init();
};

function MockClient(args) {
  if (! (this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

  this.init = function(a) {
    self.attr = a;
    if(a.freq >= 1 && a.freq <= 100){
      self.attr.interval = 1000/a.freq;
    } else {
      self.attr.interval = 1000;
    };
    self.attr.id = 'rmc_'+randomString(8);
    self.attr.spawnedAt = Date.now();
    self.attr.pause = false; // start paused?, nooooo!
    // dance and kill interface are stored here
    self.interfaces = {};
    // create channel handles we want to publish to
    self.attr.infoChannel = '/'+self.attr.id+'/information';
    self.attr.dataChannel = '/'+self.attr.id+'/data';
    self.setupBayeuxHandlers();
    console.log('MockClient: id: '+self.attr.id+', data interval: '+self.attr.interval+' ms.');
  };
  

  /* 
   *  Builds or returns the mock clients pause interface element
   *
   *  arguments: raphael,danceX,danceY 
   * */
  this.pauseInterface = function(r,x,y) {
    if(!self.interfaces.pauseInterface){
      // build interface element
      var pause = r.circle(x,y,8).attr({ stroke: 'none', fill: '#00e18f' });
      // add click event handler
      pause.click(function(e){ 
        self.attr.pause = !self.attr.pause;
        if(self.attr.pause){
          self.interfaces.pauseInterface.animate({ opacity: 0.25 }, 750);
          // stop the timers
          clearInterval(self.attr.networkTimer);
          clearInterval(self.attr.dataTimer);
        } else {
          self.interfaces.pauseInterface.animate({ opacity: 1 }, 750);
          // resume the timers
          self.startTimer();
        };
      });
      self.interfaces.pauseInterface = pause;
    } else {
      // update x,y values
      self.interfaces.pauseInterface.attr({ cx: x, cy: y });
    };
    // not paused? animate (looks nice and makes it visible)
    if(!self.attr.pause){
      self.interfaces.pauseInterface.animate({ r: 10 }, 250, '<', function() {
        this.animate({ r: 8 }, 250, '>');    
      });
    };
  };


  /* 
   *  Builds or returns the mock clients kill interface element
   *
   *  arguments: simulator,killX,killY,index
   * */
  this.killInterface = function(sim,x,y,i) {
    if(!self.interfaces.killInterface){
      // build interface element
      var kill = sim.can.circle(x,y,8).attr({ stroke: 'none', fill: '#f32ccf' });
      // add click event handler
      kill.click(function(e){ 
        // stop timers and remove self from mocks array
        clearInterval(self.attr.networkTimer);
        clearInterval(self.attr.dataTimer);
        // splice us out
        sim.mocks.splice(i, 1);
        // finally, fade away, item will be removed on next update
        self.interfaces.killInterface.animate({ opacity: 0 }, 750);
        self.interfaces.pauseInterface.animate({ opacity: 0 }, 750);
      });
      self.interfaces.killInterface = kill;
    } else {
      // update x,y values
      self.interfaces.killInterface.attr({ cx: x, cy: y });
    };
    // animate (looks nice and makes it visible)
    self.interfaces.killInterface.animate({ r: 9 }, 750, 'bounce', function() {
      this.animate({ r: 8 }, 500, '>');    
    });
  };


  /* 
   *  Start the simulator timers   
   * */
  this.startTimer = function() {
    // there are two things that have to happen periodicaly, we need to inform other nodes of our presence
    self.attr.networkTimer = setInterval(self.publishNetwork, 5000);
    // and create and send mock data over our data channel
    self.attr.dataTimer = setInterval(self.publishData, self.attr.interval);
  };
  
  
  /* 
   *  Publishes client information to network
   *
   *  channel: /network
   *  content: sender, createdAt, infoChannel, dataChannel
   * */
  this.publishNetwork = function() {
    // console.log('('+self.attr.id+'): new information!');
    var info = { sender:self.attr.id, createdAt:Date.now() };
    // add channel information
    info.infoChannel = self.attr.infoChannel;
    info.dataChannel = self.attr.dataChannel;
    // publish the information to network
    self.client.publish('/network', info);
  };
  
  /* 
   *  Publishes client data
   *
   *  channel: /id/data
   *  content: x,y,z,delta
   * */
  this.publishData = function() {
    // console.log('('+self.attr.id+'): new data!');
    var data = { createdAt:Date.now() };
    // random x,y,z
    data.x = 1 - Math.random()*2; 
    data.y = 1 - Math.random()*2; 
    data.z = 1 - Math.random()*2;
    // random delta (delta range should be 0-100)
    data.delta = Math.random()*100;
    // set message type
    data.accelerometerUpdate = true;
    // publish the client data
    self.client.publish(self.attr.dataChannel, data);
  };

  /* 
   *  The websocket/faye event listeners are setup here 
   * */
  this.setupBayeuxHandlers = function() {
    $.getJSON("/config.json", function (config) {
      self.client = new Faye.Client("http://" + window.location.hostname + ':' + config.port + '/faye', {
        timeout: 120
      });
      // send out a hello!
      self.client.publish('/global/info', { sender:self.attr.id, content:'New MockClient ('+self.attr.id+') connected!' });
      // the client is set, start the simulator loops
      self.startTimer();
    });
  };

  this.init(args);
};

var resonanceSimulator;
jQuery(function() {
  resonanceSimulator = new ResonanceSimulator();
  $(window).resize(function() {
    resonanceSimulator.viewDidResize();
  });
});
