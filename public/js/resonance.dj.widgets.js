/*
*  The Resonance Tracks widget displays the tracks with the highest crowd activity
* */
function ResonanceTracks(args) {
  if (! (this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

/*
*  Constructor
* */
  this.init = function(args) {
    self.rdj = args.rdj;
    self.svgAttr    = self.rdj.svgAttr.tracks;
    self.can        = self.rdj.can;
    self.initInterface();
    self.svgs       = [];
  };

  /* 
  *  Create interface element
  * */
  this.initInterface = function() {
    self.title = self.can.text(self.svgAttr.x, self.svgAttr.y, 'Activity | Titel (Artist // Album // BPM)');
    self.title.attr({ fill:'#00e18f', 'font-size':'30px', 'font-weight':'bold', 'font-family':'Arial', 'text-anchor':'start' });
  };

  /* 
  *  Update interface element
  * */
  this.update = function(msg) {
    // remove old svg track objects
    $.each(self.svgs, function(i, s){ s.remove(); });
    // display a maximum of 10 tracks
    var limit = 10;
    if(msg.crowdTracks.length < limit){ limit = msg.crowdTracks.length};
    // dbg('('+msg.sender+') crowd tracks: '+msg.crowdTracks.length+', limit: '+limit);
    // draw tracks
    for(var i = 0; i < limit; i++){
      var track    = msg.crowdTracks[i];
      // activity and title
      var y = self.svgAttr.y+45*(i+1);
      var svgTrack       = self.can.text(self.svgAttr.x, y, roundNumber(track.activity*100,2)+'% | '+track.track.title);
      svgTrack.attr({ fill:'#09b5e3', 'font-size':'20px', 'font-weight':'bold', 'font-family':'Arial', 'text-anchor':'start' });
      // artist // album // bpm
      var svgTrackDetail = self.can.text(self.svgAttr.x, y+20, track.track.author+' // '+track.track.album+' // '+track.track.bpm);
      svgTrackDetail.attr({ fill:'#ffde00', 'font-size':'15px', 'font-family':'Arial', 'text-anchor':'start' });
      // add svgs to this array for later clean up
      self.svgs.push(svgTrack);
      self.svgs.push(svgTrackDetail);
    };
  };

  this.init(args);
};

/*
*  The Resonance Crowd Participation widget displays the current crowd participation
* */
function ResonanceCrowdParticipation(args) {
  if (! (this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

/*
*  Constructor
* */
  this.init = function(args) {
    self.rdj = args.rdj;
    self.svgAttr    = self.rdj.svgAttr.participation;
    self.animations = self.rdj.animations;
    self.can        = self.rdj.can;
    self.participation = 0;
    self.initInterface();
    dbg('ResonanceCrowdParticipation initialized!');
  };

  /* 
  *  Create interface element
  * */
  this.initInterface = function() {
    // Custom Attribute definitions for Raphael instance
    var R = self.svgAttr.R;
    var param = self.svgAttr.param;
    self.can.customAttributes.arc = function (value, total, R) {
      var path = "";
      var alpha = 360 / total * value, 
      X = self.svgAttr.x, Y = self.svgAttr.y,
      a = (90 - alpha) * Math.PI / 180,
      x = X + R * Math.cos(a),
      y = Y - R * Math.sin(a);
      var color = '#f32ccf';
      if (total == value) {
        path = [["M", X, Y - R], ["A", R, R, 0, 1, 1, X-0.01, Y - R]];
      } else {
        path = [["M", X, Y - R], ["A", R, R, 0, +(alpha > 180), 1, x, y]];
      };
      return { path: path, stroke: color };
    };
    // draw the arcs
    self.drawMarks(R, 100);
    self.arc = self.can.path().attr(param).attr({ arc: [0, 100, R] });
    self.updateVal(self.participation, 100, R, self.arc);
    self.title = self.can.text(X+R+20, Y, 'Participation: 0%');
    self.title.attr({ fill:'#09b5e3', 'font-size':'30px', 'font-weight':'bold', 'font-family':'Arial', 'text-anchor':'start' });
  };

  this.drawMarks = function(R, total) {
    var marksAttr = {fill: "#444", stroke: "none"};
    var color = '#f32ccf';
    X = self.svgAttr.x, Y = self.svgAttr.y;
    var out = self.can.set();
    for (var value = 0; value < total; value++) {
      var alpha = 360 / total * value,
      a = (90 - alpha) * Math.PI / 180,
      x = X + R * Math.cos(a),
      y = Y - R * Math.sin(a);
      out.push(self.can.circle(x, y, 2).attr(marksAttr));
    };
    return out;
  };

  /* 
  *  Update interface element
  * */
  this.update = function(data) {
    var sum = 0;
    var p   = 0;
    $.each(data.members, function(k,v) {
      if(v > 0) { sum += 1 };
    });
    // calculate crowd participation in percent TODO: THIS IS A N=>CROWD mapping
    if(data.crowdCard > 0 && sum > 0) { 
      var p = roundNumber((100/sum)/data.crowdCard, 2);
    };
    // only redraw if we have to!
    if(p != self.participation){
      self.participation = p;
      self.title.attr('text', 'Participation: '+p+'%');
      self.updateVal(p, 100, self.svgAttr.R, self.arc);
    };
  };

/*
* Update an arc display element
* */
  this.updateVal = function(value, total, R, hand) {
    if(value <= 0 || !value) { value = 0 };
    //var color = '#f32ccf';
    hand.animate({arc: [value, total, R]}, 3500);
  };

  this.init(args);
};

/*
*  The Resonance Decks widget displays DJ's decks with currently playing tracks
* */
function ResonanceDecks(args) {
  if (! (this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

/*
*  Constructor
* */
  this.init = function(args) {
    self.rdj = args.rdj;
    self.svgAttr    = self.rdj.svgAttr.decks;
    self.can        = self.rdj.can;
    self.initInterface();
  };

  /* 
  *  Create interface element
  * */
  this.initInterface = function() {
    self.decks = {};
    // setup decks
    for(var i=1; i<5; i++) {
      self.decks[i] = new ResonanceDeck({ rd: self, index: i-1, id: i });
    };
  };

  /* 
  *  Update interface element
  * */
  this.update = function(msg) {
    self.msg = msg;
    // update all (4) decks
    for(var i=1; i<5; i++) {
      var info = msg.vdjInfo['deck '+i];
      info.timestamp  = msg.vdjInfo.timestamp;
      info.activeDeck = msg.vdjInfo.activeDeck;
      info.channel    = msg.vdjInfo.channel;
      self.decks[i].update(info);
    };
  };

  this.init(args);
};

/*
*  The Resonance Deck manages a single deck
* */
function ResonanceDeck(args) {
  if (! (this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

/*
*  Constructor
* */
  this.init = function(args) {
    self.index = args.index;
    self.id    = args.id;
    self.rd    = args.rd;
    self.svgAttr    = self.rd.svgAttr;
    self.can        = self.rd.can;
    // used to save some time related variables
    self.tcache = { min: 0, sec: 0 };
    self.initInterface();
  };

  /* 
  *  Create interface element
  * */
  this.initInterface = function() {
    dbg('initInterface: Deck '+self.id);

    // INTERFACE
    var volumePath = "m 0,0 591,0 c 0,0 4,0 4,4 0,4 -4,4 -4,4 l -533,0 c -37,0 -50,16 -50,35 0,0 -59,0 -59,0 0,-16 0,-43 50,-43 z";
    self.volume = self.can.path(volumePath);
    self.volume.attr({ stroke:'#00e18f', 'stroke-width': 2 });
    self.volume.translate(self.svgAttr.x, self.svgAttr.y + self.index*105);

    var timePath = "m 0,95 591,0 c 0,0 4,0 4,-4 0,-4 -4,-4 -4,-4 l -533,0 c -37,0 -50,-16 -50,-35 0,0 -59,0 -59,0 0,16 0,43 50,43 z";
    self.time = self.can.path(timePath);
    self.time.attr({ stroke:'#09b5e8', 'stroke-width': 2 });
    self.time.translate(self.svgAttr.x, self.svgAttr.y + self.index*105);

    self.active = self.can.circle(0, 47.5, 30);
    self.active.attr({ stroke:'#09b5e8', 'stroke-width': 2 });
    self.active.translate(self.svgAttr.x + 565, self.svgAttr.y + self.index*105);

    self.ring = self.can.circle(0, 47.5, 22.5);
    self.ring.attr({ stroke:'#00e18f', 'stroke-width': 2 });
    self.ring.translate(self.svgAttr.x + 565, self.svgAttr.y + self.index*105);

    self.play = self.can.path("m -5,32.5 0,28 18,-14 z");
    self.play.attr({ fill:'#09b5e8','fill-opacity':1, stroke:'#09b5e3','stroke-width':1,'stroke-linecap':'round','stroke-linejoin':'round' });
    self.play.translate(self.svgAttr.x + 565, self.svgAttr.y + self.index*105);

    // STATIC TEXT
    var labelStyle = { fill:'#ffde00', 'font-size':'14px', 'font-weight':'bold', 'font-family':'Arial' };
    self.volumeLabel = self.can.text(-21,34, "volume");
    self.volumeLabel.attr(labelStyle);
    self.volumeLabel.translate(self.svgAttr.x, self.svgAttr.y + self.index*105);

    self.timeLabel = self.can.text(-21,61, "time");
    self.timeLabel.attr(labelStyle);
    self.timeLabel.translate(self.svgAttr.x, self.svgAttr.y + self.index*105);

    self.bpm = self.can.text(525,47.5, "bpm");
    self.bpm.attr(labelStyle);
    self.bpm.attr({ 'font-size':'30px', 'text-anchor':'end' });
    self.bpm.translate(self.svgAttr.x, self.svgAttr.y + self.index*105);  
    // DYNAMIC TEXT
    self.title = self.can.text(0,34, "*title*");
    self.title.attr({ fill:'#09b5e3', 'font-size':'20px', 'font-weight':'bold', 'font-family':'Arial', 'text-anchor':'start' });
    self.title.translate(self.svgAttr.x + 35, self.svgAttr.y + self.index*105);

    self.detail = self.can.text(0,57, "*artist* - *album* - *key*");
    self.detail.attr({ fill:'#00e18f', 'font-size':'14px', 'font-weight':'normal', 'font-family':'Arial', 'text-anchor':'start' });
    self.detail.translate(self.svgAttr.x + 35, self.svgAttr.y + self.index*105);
  };

  /* 
  *  Update interface element
  * */
  this.update = function(u) {
    // dbg('updating deck '+self.id);
    // update the song infos
    if(u.song) {
      self.updateText(self.title, u.song.title);
      if(u.song.bpm) { self.updateText(self.bpm, u.song.bpm.replace('*','')); };
      self.updateText(self.detail, self.buildDetailText(u.song));
    } else {
      self.updateText(self.title, '');
      self.updateText(self.bpm, '');
      self.updateText(self.detail, '');
    };
    // update the volume bar
    if(u.volume) {
      var vord = 1000000000; var vmax = 65353216; // this is 100% volume (-0db)
      var frac = Math.pow(100,(u.volume-vord)/vmax); // approximation good enough
      self.volume.attr({ 'fill':'0-#00e18f:'+(frac*0.95)+'-#222:'+frac });
      // update the 'active' ring (indicating if the track is part of the mix)
      self.active.animate({ 'stroke-opacity': 1 }, 500);
    } else {
      // 0 volume
      self.volume.attr({ 'fill': '#222' });
      // update the 'active' ring (indicating if the track is part of the mix)
      self.active.animate({ 'stroke-opacity': 0 }, 500);
    };
    // update time related ui elements
    var playing = false;
    if(u.time.totaltimeMin && u.time.totaltimeSec) {
      if(u.time.timeSec) {
        if(self.tcache.min != u.time.timeMin || self.tcache.sec != u.time.timeSec) {
          self.tcache.min = u.time.timeMin;
          self.tcache.sec = u.time.timeSec;
          playing = true;
        };
        // update the time bar
        var total   = (u.time.totaltimeMin*60) + (1*u.time.totaltimeSec);
        var current = 1*u.time.timeSec;
        if(u.time.timeMin) { current += u.time.timeMin*60; };
        var frac = 100*current/total;
        self.time.attr({ 'fill': ('0-#09b5e3:'+(frac*0.95)+'-#222:'+frac) });
      } else {
        self.time.attr({ 'fill': '#222' });
      };
    };
    // update the 'playing' indicator
    if(playing) {
      self.play.animate({ 'fill-opacity': 1, 'stroke-opacity': 1 }, 500);
    } else {
      self.play.animate({ 'fill-opacity': 0, 'stroke-opacity': 0 }, 500);
    };
  };

/*
*  Update a svg text element
*  TODO: elipsis
* */
  this.updateText = function(svg, text) {
    if(svg) {
      var txt = "..."; // fallback
      if(text && text != "") {
        txt = text;
      };
      svg.attr({ text: txt });
    };
  };

/*
*  Build the 'detail' text from given song infos
* */
  this.buildDetailText = function(song) {
    var detail = "";
    if(song.author && song.author != "") { detail += song.author; };
    if(song.album && song.album != "") { 
      if(detail!="") { detail += " // "}; // add a seperator
      detail += song.album; 
    };
    if(song.key && song.key != "") { 
      if(detail!="") { detail += " // "}; // add a seperator
      detail += song.key; 
    };
    return detail;
  };

  this.init(args);
};



/*
*  The Resonance Plotter plots the current crowd dance activity
* */
function ResonancePlotter(args) {
  if (! (this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

/*
*  Constructor
* */
  this.init = function(args) {
    self.rdj = args.rdj;
    self.svgAttr    = self.rdj.svgAttr.plotter;
    self.animations = self.rdj.animations;
    self.can        = self.rdj.can;
    self.initInterface();
  };

  /* 
  *  Create interface element
  * */
  this.initInterface = function() {
    // the plot dot default style
    self.svgAttr.plotDot = { stroke: 'none', fill: '#ffde00' };
    // we'll store the dot information here
    self.data = { current: 0, dots: [], dotSet: self.can.set() };
    // dot radius therefore is
    var dotRad = roundNumber((self.svgAttr.width/(self.svgAttr.resolution)/2),2);
    self.svgAttr.dotRad = dotRad;
    // function used to compute dot X
    var dotX = function(index){ return (self.svgAttr.x + 2*index*dotRad + dotRad); };
    for(var i=0; i < self.svgAttr.resolution; i++){
      var iDot  = self.can.circle(dotX(i),(self.svgAttr.y + self.svgAttr.height - 2*dotRad),dotRad);
      self.data.dotSet.push(iDot);
      self.data.dots.push(iDot);
    };
    // reverse dot order to animate from right to left
    if(self.animations.dotReverse) {
      self.data.dots.reverse();
    };
    // style all dots
    self.data.dotSet.attr(self.svgAttr.plotDot);
  };

  /* 
  *  Update interface element
  * */
  this.update = function(delta) {
    // update current plot dot
    var currentDot = self.data.dots[self.data.current];
    // the distance from canvas top to plot bottom
    var b = self.svgAttr.y + self.svgAttr.height - 2*self.svgAttr.dotRad;
    // delta values are between 0..100, 50 is supposed to equal dancing
    var m = self.svgAttr.height / 100; 
    // y is inverted, therefore
    var y = roundNumber((b - m * delta ), 2);
    // select peak color, ordered by color temperature
    if(delta < 25){ peakColor = '#ffde00'; // 3100 K
    } else if(delta >= 25 && delta < 50) { peakColor = '#00e18f'; // 3700 K
    } else if(delta >= 50 && delta < 75) { peakColor = '#09b5e3'; // 5000 K
    } else if(delta >= 75) { peakColor = '#f32ccf' }; // 12000 K 
    currentDot.animate({ cy: y, fill: peakColor }, self.animations.dotRise, '>', function() {
      // let them come down slowly and change fill back to #ffde00
      this.animate({ cy: b, fill: '#ffde00' }, self.animations.dotFall, '<');
    });
    // set new current dot
    self.data.current = (self.data.current+1) % (self.data.dots.length);
  };

  this.init(args);
};
