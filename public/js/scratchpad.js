function ResonanceDummy(args) {
  if (! (this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

  /*
   *  Constructor
   * */
  this.init = function(args) {
    debug = self;
    self.rdj = args.rdj;
    self.svgAttr    = self.rdj.svgAttr.dummy;
    self.animations = self.rdj.animations;
    self.can        = self.rdj.can;
    self.initInterface();
  };

  /* 
   *  Create interface element
   * */
  this.initInterface = function() {

  };

  /* 
   *  Update interface element
   * */
  this.update = function() {

  };

  this.init(args);
};
