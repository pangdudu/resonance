function ResonanceButton(args) {
  if (! (this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

  /*
   *  Constructor
   * */
  this.init = function(args) {
    self.settings = args;
    self.svg = {};
    self.draw();
    self.addEventHandlers();
  };
  
  this.draw = function() {
    self.active = false;
    var s = self.settings;
    var svg = self.svg;
  
    svg.background = s.canvas.rect(s.x, s.y, s.width, s.height, 10);
    svg.text       = s.canvas.text(s.x+s.width/2, s.y+s.height*0.5, s.text);
    svg.foreground = s.canvas.rect(s.x, s.y, s.width, s.height, 10);
    
    svg.background.attr({ fill: s.color, 'fill-opacity': 0, stroke: s.color, 'stroke-width': 2 });
    svg.text.attr({ fill: s.color, 'font-size': s.height*0.7 });
    svg.foreground.attr({ fill: '#000', 'fill-opacity': 0, stroke: 'none' });
  };

  this.activate = function() {
    self.active = true;
    self.svg.background.animate({ 'fill-opacity': 0.15, 'stroke-width': 3 }, 250, '>');
  };

  this.deactivate = function() {
    self.active = true;
    self.svg.background.animate({ 'fill-opacity': 0, 'stroke-width': 2 }, 250, '>');
  };

  this.addEventHandlers = function() {
    // this is where we later attach the event handlers
    self.node = $(self.svg.foreground.node);
    // nice hover effect
    self.node.hover(function(e) {
        self.svg.background.animate({ 'fill-opacity': 0.3 }, 250, '>');
      }, function(e) {
        if(self.active){ 
            self.svg.background.animate({ 'fill-opacity': 0.15 }, 250, '<');
          } else {
            self.svg.background.animate({ 'fill-opacity': 0 }, 250, '<');
        };
      }
    );
  };


  this.init(args);
};


  /* 
   *  Adds a html text input field
   *
   *  style: text.css
   *  to get/set value: $('.domid .txt').attr('value','');
   *  cached: self.dom('.domid .txt').attr('value','');
   * */
  this.addTextInput = function(x,y,width,domid) {
    // create text input element, 
    // TODO: using class to identify elements cause id returns no elements 
    $('#textinputs .relative_container').append(
      '<div class="'+domid+'"><input type="text" class="txt edge" value=""></div>'
    );
    var textInput = self.dom('.'+domid);
    textInput.css({
      'position': 'absolute'
      ,'left': x+'px'
      ,'top': y+'px'
      ,'width': width+'px'
    });
  };


