/*
 *  Round a number, also accepts decimal precision as second argument
 * */
var roundNumber = function(num,precision){
  var dec = 3;
  if(parseInt(precision) > 0){ dec = precision };
  var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
  return result;
}

/*
 *  Benchmarking method
 * */
var bench = function(id) {
  if(benchmarks[id]){
    var runtime = Date.now() - benchmarks[id];
    benchmarks[id] = false; 
    console.log('BENCHMARK ('+id+'): '+runtime+' ms.');
  } else {
    benchmarks[id] = Date.now();
  }
};  
var benchmarks = {};

/* 
 *  Simple debug output
 * */
var dbg = function(txt) {
  if(debug){ 
    if(txt){
      console.log('DBG('+(new Date())+'): '+txt);
    };
  };
};
var debug = true;

// gives back a random string
var randomString = function(length) {
  var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');
    
  if (! length) {
    length = Math.floor(Math.random() * chars.length);
  }
    
  var str = '';
  for (var i = 0; i < length; i++) {
    str += chars[Math.floor(Math.random() * chars.length)];
  }
  return str;
}
