function ResonanceDJ() {
  if (! (this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

  /*
   *  Constructor
   * */
  this.init = function() {
    self.svg           = {}; // storing raphael svg objects in here
    self.svgAttr       = {}; // storing raphael svg object attributes in here
    self.animations    = { dotRise: 375, dotFall: 3750, dotReverse: false }; // animation associated variables go in here
    self.dom_cache     = {}; // cache for dom elements
    self.timers        = {}; // we store our timer ids in here
    self.settings      = { id: 'rdj_'+randomString(8), networkInterval: 5000 }; // faye settings
    self.setSvgAttr();
    // now initialize all the goodness we need
    self.initInterface();
    self.setupBayeuxHandlers();
    debug = self;
  };

  /*
   *  Set some SVG attributes for our elements
   * */
  this.setSvgAttr = function() {
    self.svgAttr.canvas        = { width: 1680, height: 1050 };
    self.svgAttr.helperBox     = { stroke: '#f32ccf', 'stroke-width': 4, 'stroke-opacity': 1 };
    self.svgAttr.participation = { x: 300, y: 275, R: 100, param: { stroke: "#fff", "stroke-width": 20 } };
    self.svgAttr.plotter       = { x: 910, y: 138, width: 700, height: 400, resolution: 120 };
    self.svgAttr.decks         = { x: 980, y: 590 };
    self.svgAttr.tracks        = { x: 160, y: 490 };
  };

  /* 
   *  Create interface elements 
   * */
  this.initInterface = function() {
    self.drawBackground();
    // now setup the interface widgets 
    self.participation = new ResonanceCrowdParticipation({ rdj: self });
    self.plotter       = new ResonancePlotter({ rdj: self });
    self.decks         = new ResonanceDecks({ rdj: self });
    self.tracks        = new ResonanceTracks({ rdj: self });
    
    // HELPER BOXES
    // orientation help for the crowd tracks box
    //self.svg['crowdTracksBox'] = self.can.rect(150,480,660,510,15);
    //self.svg.crowdTracksBox.attr(self.svgAttr.helperBox);
    
    self.viewDidResize();
  };

  /*
   *  Create raphael canvas and draw the background
   * */
  this.drawBackground = function() {
    // create the raphael canvas
    self.can = Raphael('canvas', self.svgAttr.canvas.width, self.svgAttr.canvas.height);
    self.can.canvas.setAttribute('viewBox', '0 0 '+self.svgAttr.canvas.width+' '+self.svgAttr.canvas.height);
    self.can.image("/img/resonance_dj_background.png", 0, 0, self.svgAttr.canvas.width, self.svgAttr.canvas.height);
  };

  /*
   *  Get's called if view was resized
   * */
  this.viewDidResize = function () {
    var width        = self.dom('body').width();
    var windowHeight = $(window).height();
    var canvasHeight = width * (self.svgAttr.canvas.height / self.svgAttr.canvas.width);
    var marginTop    = (windowHeight - canvasHeight) / 2.0;
    // by doing this, we're not in trouble if the canvas was originaly higher than the window
    if(canvasHeight > windowHeight){
      canvasHeight = windowHeight;
      marginTop    = 0;
    };
    self.can.setSize(width, canvasHeight);
    self.dom('#canvas').css({
      'margin-top': marginTop
    });
  };

  /* 
   *  The websocket/faye event listeners are setup here 
   * */
  this.setupBayeuxHandlers = function() {
    $.getJSON("/config.json", function (config) {
      self.client = new Faye.Client("http://" + window.location.hostname + ':' + config.port + '/faye', {
        timeout: 120
      });
      
      // global information channel
      self.client.subscribe('/global/info', function(msg) {
        dbg('('+msg.sender+'): '+msg.content); 
      });
      // global crowd participation channel, e.g. global crowd
      self.client.subscribe('/global/crowd', function(msg) {
        // if a plotter has been initialized
        if(self.participation) {
          //dbg('('+msg.sender+') crowd: '+msg.crowdCard); 
          self.participation.update(msg);
        };
      });
      // global crowd top tracks channel, e.g. global crowd tracks
      self.client.subscribe('/global/crowdTracks', function(msg) {
        // if a plotter has been initialized
        if(self.tracks) {
          //dbg('('+msg.sender+') crowd tracks: '+msg.crowdTracks.length);
          self.tracks.update(msg);
        };
      });
      // global dance activity channel, e.g. global delta
      self.client.subscribe('/global/delta', function(msg) {
        // if a plotter has been initialized
        if(self.plotter) {
          self.plotter.update(roundNumber(msg.delta, 2));
        };
      });
      // global VDJ api channel (track infos)
      self.client.subscribe('/global/api/vdj', function(msg) {
        // if a decks widget has been initialized
        if(self.decks) {
          self.decks.update(msg);
        };
      });

      // send out a hello to everybody!
      self.client.publish('/global/info', { sender: self.settings.id, content: 'ResonanceDJ ready to go!' });
      // create the channel handles that we want to publish to
      self.settings.infoChannel = '/'+self.settings.id+'/information';
      self.settings.dataChannel = '/'+self.settings.id+'/data';
      // start the timers
      self.startTimer();
    });
  };

  /* 
   *  Publishes client information to network
   *
   *  channel: /network
   *  content: sender, createdAt, infoChannel, dataChannel
   * */
  this.publishNetwork = function() {
    var info = { sender:self.settings.id, createdAt:Date.now() };
    // add channel information
    info.infoChannel = self.settings.infoChannel;
    info.dataChannel = self.settings.dataChannel;
    // publish the information to network
    self.client.publish('/network', info);
  };

  /* 
   *  Start the timers   
   * */
  this.startTimer = function() {
    self.timers['network'] = setInterval(self.publishNetwork, self.settings.networkInterval);
  };

  /* 
   *  Simple cache for dom elements 
   * */
  this.dom = function(cssId) {
    var domElement;
    if(!(domElement = self.dom_cache[cssId])){
      domElement = $(cssId);
    };
    return domElement;
  };

  this.init();
};

// INITIALIZE!
var resonanceDJ;
jQuery(function() {
  resonanceDJ = new ResonanceDJ();
  $(window).resize(function() {
    resonanceDJ.viewDidResize();
  });
});
// used for debug
var debug;
