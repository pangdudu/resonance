function Dancer(){
  if (!(this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }

  var self = this;

  this.init = function(){
    self.attr = {id:'dancer'};
    MessageListeners = [];
    // we cache DOM elements in here
    self.cache = {};
    // storage support
    self.attr.defaults   = [];
    self.attr.properties = [];
    self.storage         = new StorageManager(self.attr);
    // faye pub/sub client support
    self.faye            = new FayeClient({id:'faye_client'});
    self.addEventListeners();
    self.initInterface();
    // dbgc('('+self.attr.id+')Dancer initiated!');
  }
  
  this.initInterface = function(){
    // Info tab
    if(!scratchbox){
      self.dance_detector = new DanceDetector({id:'dance_detector'});
    } else {
      alert('SCRATCHBOX: NO ACCELEROMETER!');
    }
    self.message_scroller = new MessageScroller({id:'message_scroller', container:'#messages', message_tag:'all'});
    // Feedback tab
    self.current_bpm_slider = new Slider({id:'current_bpm', scale_min:64, scale_max:196});
    self.desired_bpm_slider = new Slider({id:'desired_bpm', scale_min:64, scale_max:196});
    self.track_manager = new TrackManager({id:'track_manager'});
    // Settings tab
    self.connection_handler = new ConnectionHandler({id:'connection_handler', faye:self.faye});
    self.dbgMembers();
    self.switchTabs(0,1,2);
  }

  this.dbgMembers = function(){
    dbgc('('+self.attr.id+')self.dance_detector: '+self.dance_detector);
    dbgc('('+self.attr.id+')self.message_scroller: '+self.message_scroller);
    dbgc('('+self.attr.id+')self.current_bpm_slider: '+self.current_bpm_slider);
    dbgc('('+self.attr.id+')self.desired_bpm_slider: '+self.desired_bpm_slider);
    dbgc('('+self.attr.id+')self.track_manager: '+self.track_manager);
    dbgc('('+self.attr.id+')self.connection_handler: '+self.connection_handler);
  }

  this.addEventListeners = function(){
    // need this, so touchmoves aren't interpreted
	  document.addEventListener("touchmove", self.preventBehavior, false);
    
    /*
     *  Menu buttons
     * */
    x$('#btn_info').click(function(e){
      self.switchTabs(0,1,2);   // bring info tab to the front
    });
    x$('#btn_feedback').click(function(e){
      self.switchTabs(-1,0,1);  // bring feedback tab to the front
    });
    x$('#btn_settings').click(function(e){
      self.switchTabs(-2,-1,0); // bring settings tab to the front
    });
    
    /*
     *  BPM slider events in the 'Feedback' tab 
     *  (legacy events work in the sim, might use touch equivalents on phone)
     * */
    x$('#desired_bpm .knob').on('mousedown', function(e){
      self.desired_bpm_slider.activate();
    });    
    x$('#feedback').on('mouseup', function(e){
      self.desired_bpm_slider.deactivate();
    });
    // move should maybe be attached to the parent div
    x$('#feedback').on('mousemove', function(e){
      self.desired_bpm_slider.movement(e);
    });
    /*
     *  'Like this track' button in the 'Feedback' tab
     * */
    x$('#btn_like').click(function(e){
      if(!self.cache.btn_like){ self.cache['btn_like'] = x$('#btn_like').first(); }
      emile(self.cache.btn_like, 'background: #333; color: #ffde00'
        ,{duration: 150, after: function(){
          emile(self.cache.btn_like, 'background: #444; color: #f32ccf', {duration: 150});
        }
      });
      self.track_manager.like();
    });
    /*
     *  'Save settings' button in the 'Settings' tab
     * */
    x$('#btn_save').click(function(e){
      if(!self.cache.btn_save){ self.cache['btn_save'] = x$('#btn_save').first(); }
      emile(self.cache.btn_save, 'background: #333; color: #ffde00'
        ,{duration: 150, after: function(){
          emile(self.cache.btn_save, 'background: #444; color: #f32ccf', {duration: 150});
        }
      });
      if(!self.cache.username){ self.cache['username'] = document.SettingsForm.UsernameField.value; }
      self.connection_handler.setUsername(self.cache.username);
    });
    /*
     *  'Connect' button in the 'Settings' tab
     * */
    x$('#btn_connect').click(function(e){
      if(!self.cache.btn_connect){ self.cache['btn_connect'] = x$('#btn_connect').first(); }
      emile(self.cache.btn_connect, 'background: #333; color: #ffde00'
        ,{duration: 150, after: function(){
          emile(self.cache.btn_connect, 'background: #444; color: #f32ccf', {duration: 150});
        }
      });
      if(!self.cache.server){ self.cache['server'] = document.ServerForm.ServerField.value; }
      self.connection_handler.connect(self.cache.server);
    });
  }

  /*
   *  Switch tab to another tab
   * */
  this.switchTabs = function(info,feedback,settings){
    var transit_duration = 300;
    if(landscape){
      var css = 'top';
      if(!self.cache.change){ self.cache['change'] = window.innerHeight; }
    } else {
      var css = 'left';
      if(!self.cache.change){ self.cache['change'] = window.innerWidth; }
    }

    if(!self.cache.info_tab){ self.cache['info_tab'] = x$('#info.tab'); }
    emile(self.cache.info_tab.first(), css+': '+(info*self.cache.change)+'px'
        , {duration: transit_duration
        , after: function(){
          self.cache.info_tab.setStyle('display','block'); // first time we start tabs are hidden
        }
    });
    if(!self.cache.feedback_tab){ self.cache['feedback_tab'] = x$('#feedback.tab'); }
    emile(self.cache.feedback_tab.first(), css+': '+(feedback*self.cache.change)+'px'
        , {duration: transit_duration
        , after: function(){
          self.cache.feedback_tab.setStyle('display','block'); // first time we start tabs are hidden
        }
    });
    if(!self.cache.settings_tab){ self.cache['settings_tab'] = x$('#settings.tab'); }
    emile(self.cache.settings_tab.first(), css+': '+(settings*self.cache.change)+'px'
        , {duration: transit_duration
        , after: function(){
          self.cache.settings_tab.setStyle('display','block'); // first time we start tabs are hidden
        }
    });
    // highlight selected button
    if(info == 0){
      if(!self.cache.btn_info){ self.cache['btn_info'] = x$('#btn_info').first(); }
      self.dance_detector.animate = true; // animate x,y,z bars
      emile(self.cache.btn_info, 'background: #333; color: #ffde00', {duration: 150});
    } else { 
      self.dance_detector.animate = false; // stop animating if tab is not visible
      emile(self.cache.btn_info, 'background: #444; color: #f32ccf', {duration: 150}); 
    }
    
    if(feedback == 0){ 
      if(!self.cache.btn_feedback){ self.cache['btn_feedback'] = x$('#btn_feedback').first(); }
      emile(self.cache.btn_feedback, 'background: #333; color: #ffde00', {duration: 150});
    } else { emile(self.cache.btn_feedback, 'background: #444; color: #f32ccf', {duration: 150}); }   
    
    if(settings == 0){ 
      if(!self.cache.btn_settings){ self.cache['btn_settings'] = x$('#btn_settings').first(); }
      emile(self.cache.btn_settings, 'background: #333; color: #ffde00', {duration: 150});
    } else { emile(self.cache.btn_settings, 'background: #444; color: #f32ccf', {duration: 150}); }
  }

  /* 
   * phonegap feedback functions
   */
  this.beep = function(){
	  navigator.notification.beep(2);
  }
	
  this.vibrate = function(){
  	navigator.notification.vibrate(250);
  }
	
  this.blink = function(){
    navigator.notification.blink(10, 0xffffff);
  }

  this.preventBehavior = function(e){ 
    e.preventDefault(); 
  }
  
  this.init();
};



/*
 *  Storage Manager class
 * */
function StorageManager(args){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }

  var self = this;

  this.init = function(a){
    self.attr     = a;
    self.check();
    dbgc('('+self.attr.id+')StorageManager initiated.');
  }

  // store a property in the persistent storage
  this.store = function(key, value){
    if(DB){
      // dbgc('('+self.attr.id+')Storage: Storing: '+key+'=>'+value);
      var identifier = self.attr.id + '_persistent_properties';
      DB.find('r.identifier == "'+identifier+'"', function(r){
        r[key] = value;
        DB.save(r);
        dbgc('('+self.attr.id+')Storage: Saved: '+key+'=>'+value);
      });
    }
  }
  
  // check if the persistent storage for this object is created or do so
  this.check = function(){
    if(DB){
      var identifier = self.attr.id + '_persistent_properties';
      dbgc('('+self.attr.id+')Storage: Checking DB for: '+identifier);
      self.attr.found       = false;
      var then = Date.now();
      DB.find('r.identifier == "'+identifier+'"', function(r){ 
          dbgc('('+self.attr.id+')Storage: Found DB for: '+identifier+' in '+(Date.now()-then)+' ms.');
          self.attr.found = true; 
          self.load();
      });
      // if after a specific time, no record in the database was found, initialize the DB
      setTimeout(self.initializeDatabase, 10000);
    }
  }
  
  // initialize the database with defaults
  this.initializeDatabase = function(){
    if(DB){
      // if no storage was found, create a new one
      if(!self.attr.found){
        dbgc('('+self.attr.id+')Storage: Checked, no prior DB found, initializing DB!');
        // will be used, if storage hasn't been created
        var identifier  = self.attr.id + '_persistent_properties';
        var storage     = { identifier:identifier, created_at:Date.now() };
        var defaults    = self.attr.defaults;
        for(i in defaults){
          var def = defaults[i];
          storage[def.key] = def.value; 
        }
        DB.save(storage);
        // load settings into interface elements
        self.load();
      } else {
        dbgc('('+self.attr.id+')Storage: prior DB found.');
      }
    }
  }

  // load properties from the persistent store and update the interface
  this.load = function(){
    var properties = self.attr.properties;
    if(DB){
      dbgc('('+self.attr.id+')Storage: Loading properties.');
      var identifier = self.attr.id + '_persistent_properties';
      DB.find('r.identifier == "'+identifier+'"', function(r){
        for(i in properties){
          var property = properties[i];
          if(r[property.key]){ 
            property.func(r[property.key]);
            dbgc('('+self.attr.id+')Storage: Load: '+r[property.key]);
          }
        }
      });
    } else { // if we have no db, load defaults instead
      for(i in properties){
        var property = properties[i];
        if(self.attr.defaults[property.key]){ property.func(self.attr.defaults[property.key]); }
      }
    }
  }

  this.init(args);
}



/*
 *  MessageHandler class
 * */
function MessageHandler(args){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }
  var self = this;

  this.init = function(a){
    self.attr     = a;
    self.attr.id  = self.attr.creator_id+'_message_handler' 
    self.attr.default_lifetime = 1000 * 60 * 30; // message expires after 30 minutes
    self.messages = []; // messages are temporarly stored here
    self.reloadMessages();
    MessageListeners.push(self); // gets calles when there is a new message
    dbgc('('+self.attr.id+')MessageHandler initiated for: '+self.attr.creator_id);
  }

  /*
   *  Trigger listener functions for new messages
   * */
  this.newMessage = function(){
    for(i in MessageListeners){ 
      var listener = MessageListeners[i];
      if(listener.reloadMessages){ listener.reloadMessages(); }
    }
  }

  /* 
   * create a new message and store it
   *
   * triggers: 'reloadMessages', which updates the messages array
   *
   * msg should have: {content:x}
   * */
  this.createMessage = function(msg){
    msg.type         = 'message';
    msg.created_by   = self.attr.creator_id;
    msg.created_at   = Date.now();
    if(!msg.expire_at){ msg.expire_at = Date.now() + self.attr.default_lifetime; }
    if(!msg.tag){ msg.tag = 'info'; }
    if(DB){
      DB.save(msg);
    } else {
      if(self.filterByTag(msg)){ self.messages.push(msg); }
    }
    self.newMessage(); // trigger a new message event
  }

  /*
   *  reload messages from persistent storage and delete expired messages
   * */
  this.reloadMessages = function(){
    if(DB){
      self.messages = [];
      DB.find('r.type == "message"', function(r){
        // check if the message has expired
        var expired = (r.expire_at <= Date.now());
        if (expired){
          DB.remove(r.key);
        } else {
          if(self.filterByTag(r)){ self.messages.push(r); }
        }
      });
    } else {
      var new_messages = [];
        for(i in self.messages){
        var msg = self.messages[i];
        // check if the message has expired
        var expired = (msg.expire_at <= Date.now());
        if(!expired){
          if(self.filterByTag(msg)){ new_messages.push(msg); }
        }
      }
      self.messages = new_messages;
    }
  }

  // Filters messages based on their tag (TODO: should be tags)
  this.filterByTag = function(msg){
    var pass = false;
    if(self.attr.message_tag == 'all'){
      pass = true;
    } else if(msg.tag == self.attr.message_tag){
      pass = true;
    }
    return pass;
  }

  this.init(args);
}



/*
 *  Dance detector and activity display class
 * */
function DanceDetector(args){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }
  var self = this;

  this.init = function(a){
    self.attr     = a;
    self.animate             = true; // don't animate the bars, if they are not visible
    self.attr.max_height     = 270; // TODO: (hardcoded for n900) 270px is max
    self.attr.frequency      = 100; // try to update accelerometer with 10Hz/100ms
    self.attr.delta_interval = 1 * self.attr.frequency; // compute delta over 10 seconds
    self.attr.dance_lim      = 50; // delta sum value where 'dancing' starts
    // we cache DOM elements in here
    self.cache = {};
    self.deltas = [];
    self.delta_sum = 0;
    self.dancing = false; // current dance state
    self.messageHandler = new MessageHandler({creator_id:self.attr.id, message_tag:'dance'});
    self.watchAccel();
    dbgc('('+self.attr.id+')DanceDetector initialized!');
  }

  // watch for accelerometer changes
  this.watchAccel = function(){
    // bar transition time 
    var edur = self.attr.frequency;
      
    var suc = function(a){
      // reduce decimal places to 3
      var x = roundNumber(a.x/1000);
      var y = roundNumber(a.y/1000);
      var z = roundNumber(a.z/1000);
      
      if(self.animate){
        // need _rel values from 0  to 1 
        var x_height = self.attr.max_height * Math.abs(x);
        var y_height = self.attr.max_height * Math.abs(y);
        var z_height = self.attr.max_height * Math.abs(z);
        // let's animate the bars
        if(!self.cache.barx){ self.cache['barx'] = x$('#barx').first(); }
        if(!self.cache.bary){ self.cache['bary'] = x$('#bary').first(); }
        if(!self.cache.barz){ self.cache['barz'] = x$('#barz').first(); }
        emile(self.cache.barx, 'height: '+ x_height +'px', {duration: edur});
        emile(self.cache.bary, 'height: '+ y_height +'px', {duration: edur});
        emile(self.cache.barz, 'height: '+ z_height +'px', {duration: edur});
      }
      // display the text
      if(!self.cache.delta_length){ self.cache['delta_length'] = x$('#delta_length').first(); }
      self.cache.delta_length.innerText = self.deltas.length;
      if(!self.cache.delta){ self.cache['delta'] = x$('#delta').first(); }
      if(self.delta_sum > self.attr.dance_lim){
        self.cache.delta.innerText = 'dancing!';
        self.danceActivity(true);
      } else { 
        self.cache.delta.innerText = self.delta_sum;
        self.danceActivity(false);
      }

      // compute new delta
      self.delta({x:x, y:y, z:z, delta:0});
  	};
    
    var fail = function(){};
  	
    var opt = { frequency:self.attr.frequency };
  	
    timer = navigator.accelerometer.watchAcceleration(suc,fail,opt);
  }
  
  /*
   *  Computes the summed delta over a specified time interval
   *
   *  v: {x:double, y:double, z:double, delta:double}
   *  deltas: [v1,v2,...vn]
   * */
  this.delta = function(v){
    // remove top elements to keep the interval in bound
    while(self.deltas.length > self.attr.delta_interval-1){
      var old = self.deltas.shift();
      // delete old deltas
      self.delta_sum = self.delta_sum - old['delta'];
    }
    // compute new delta value
    if(self.deltas.length > 0){
      // get last known values
      var last = self.deltas[self.deltas.length-1];
      // compute new delta
      v['delta'] = Math.abs(last['x'] - v['x']) + Math.abs(last['y'] - v['y']) + Math.abs(last['z'] - v['z']);
      // add new delta to delta_sum
      self.delta_sum = roundNumber(self.delta_sum + v['delta']);
    }
    // add the value to the deltas array
    self.deltas.push(v);
  }

  /*
   *  Manages current dance activity state
   * */
  this.danceActivity = function(dancing){
    // if their is an activity change
    if(dancing != self.dancing){
      if(dancing){
        self.danceInformationMessage('dancing');
      } else {
        self.danceInformationMessage('notdancing');
      }
      self.dancing = dancing;
    }
  }
  
  // create dance information messages
  this.danceInformationMessage = function(dance_info){
    var msg = { tag:'dance' };
    msg.expire_at = Date.now() + 1000 * 1; // msg expires after 1 seconds
    
    if(dance_info == 'dancing'){
      msg.content = 'You started dancing!';
    } else if(dance_info == 'notdancing'){
      msg.content = 'You are not dancing ...';
    } 
    
    self.messageHandler.createMessage(msg);
  }

  this.init(args);
}



/*
 *  Message scroller class
 * */
function MessageScroller(args){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }
  var self = this;

  this.init = function(a){
    self.attr     = a;
    self.attr.update_interval = 10000; // updates text every 10 seconds 
    self.messageHandler = new MessageHandler({creator_id:self.attr.id, message_tag:self.attr.message_tag}); // handles the message data part
    self.startTimer(self.attr.update_interval);
    self.updateInterface();
    dbgc('('+self.attr.id+')MessageScroller initiated.');
  }

  /* 
   *  Starts a periodic timer that updates the message field every n seconds
   * */
  this.startTimer = function(interval){
    self.attr.timer_id = setInterval(self.updateInterface, interval);
  }

  this.updateInterface = function(){
    // TODO: should be done through .getStyle('color')
    var text_color       = '#f32ccf';
    var background_color = '#222';
    var msg = self.messageHandler.messages.shift();
    // if we have a new message to display
    if(msg){
      self.messageHandler.messages.push(msg); // add msg to the message cue again
    } else {
      msg = {content:'no messages ...',created_at:Date.now()};
    }
    var msg_text = msg.content+'\n'+datetime(msg.created_at);
    // change text in the container
    emile(x$(self.attr.container + ' .text').first()
      ,'color: '+background_color
      ,{duration: 250
        ,after: function(){
          x$(self.attr.container + ' .text').first().innerText = msg_text;
          emile(x$(self.attr.container + ' .text').first(),'color: '+text_color,{duration: 250});
        }
    });
  }

  this.init(args);
}



/*
 *  Slider class
 * */
function Slider(args){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }
  var self = this;

  this.init = function(a){
    a.active      = false;
    a.knob_width  = parseInt(x$('#'+a.id+' .knob').getStyle('width'));
    a.scale_delta = a.scale_max - a.scale_min;
    self.attr     = a;
    // DOM element cache
    self.cache = {};
    self.attr.defaults   = [{ key:'frac', value:0.5 }];
    self.attr.properties = [{ func:self.setToFrac, key:'frac' }];
    self.storage         = new StorageManager(self.attr);
    dbgc('('+self.attr.id+')Slider initiated.');
  }

  this.activate = function(){
    self.attr.active = true;
  }
  
  this.deactivate = function(){
    self.attr.active = false;
    self.storage.store('frac', self.attr.frac);
  }

  this.movement = function(e){
    if(self.attr.active){
      if(!self.cache.slider){ self.cache['slider'] = x$('#'+ self.attr.id + ' .slider');}
      var width  = parseInt(self.cache.slider.getStyle('width'));
      var kw2    = self.attr.knob_width/2;
      if(landscape){
        if(!self.cache.delta){ self.cache['delta'] = 210 + kw2;}
      } else {
        if(!self.cache.delta){ self.cache['delta'] = (window.innerWidth - width)/2 + kw2;}
      }
      var newpos = e.pageX - self.cache.delta;
      // check if the new position is within bounds
      if( newpos >= (-1*kw2) && newpos <= (width - kw2) ){
        self.attr.frac = roundNumber( (newpos + kw2)/width );
        self.updateInterface(newpos);
      }
    }
  }

  // set to a fraction of the sliders range
  this.setToFrac = function(f){
    if(!self.cache.slider){ self.cache['slider'] = x$('#'+ self.attr.id + ' .slider');}
    var width  = parseInt(self.cache.slider.getStyle('width'));
    var newpos = f * width;
    self.attr.frac = f;
    self.updateInterface(newpos);
  }

  // set to a specific BPM count within the sliders range
  this.setToBPM = function(bpm){
    if(bpm >= self.attr.scale_min && bpm <= self.attr.scale_max){
      if(!self.cache.slider){ self.cache['slider'] = x$('#'+ self.attr.id + ' .slider');}
      var f      = roundNumber((bpm-self.attr.scale_min)/self.attr.scale_delta);
      var width  = parseInt(self.cache.slider.getStyle('width'));
      var newpos = f * width;
      self.attr.frac = f;
      self.updateInterface(newpos);
    }
  }
  
  this.updateInterface = function(newpos){
    // move the knob
    if(!self.cache.knob){ self.cache['knob'] = x$('#'+ self.attr.id + ' .knob').first();}
    emile(self.cache.knob, 'left: '+ newpos +'px', {duration: 25});
    // update the slider text
    var text = parseInt(self.attr.scale_min + self.attr.scale_delta * self.attr.frac);
    if(!self.cache.text){ self.cache['text'] = x$('#'+ self.attr.id + ' .text').first();}
    self.cache.text.innerText = text;
  }

  this.init(args);
}



/*
 *  Track Manager class
 *
 *  handles track likes and displays currently played tracks
 * */
function TrackManager(args){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }
  var self = this;

  this.init = function(a){
    self.attr     = a;
    self.messageScroller = new MessageScroller({id:'track_message_scroller'
        ,container:'#track_information', message_tag:'track'});
    self.attr.defaults   = [];
    self.attr.properties = [];
    self.storage         = new StorageManager(self.attr);
    self.current_tracks = []; // currently playing tracks
    self.trackInformationMessage('current');
    dbgc('('+self.attr.id+')TrackManager initiated.');
  }
  
  // 'like' the currently playing track
  this.like = function(){
    self.trackInformationMessage('like');
  }

  // create track information messages
  this.trackInformationMessage = function(track_info){
    var msg = { tag:'track' };
    msg.expire_at = Date.now() + 1000 * 10; // msg expires after 10 seconds
    // concat track titles to one string
    var tracks = '';
    for(i in self.current_tracks){ tracks = tracks +'"'+ self.current_tracks[i] +'"\n'; }
    if(tracks == ''){ tracks = '"no track"'; }
    
    if(track_info == 'current'){
      msg.content   = 'Currently playing: ' + tracks;
    } else if(track_info == 'like'){
      msg.content   = 'You like: ' + tracks;
    } 
    
    self.messageScroller.messageHandler.createMessage(msg);
  }

  this.init(args);
}



/*
 *  Connection Handler class
 * */
function ConnectionHandler(args){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }

  var self = this;

  this.init = function(a){
    self.attr     = a;
    self.faye     = self.attr.faye; // faye client
    // we cache DOM elements in here
    self.cache = {};
    self.messageScroller = new MessageScroller({id:'connection_message_scroller'
        ,container:'#connection_status', message_tag:'connection'});
    self.attr.defaults   = [{key:'username', value:'funkmaster'}, {key:'server', value:'localhost'}];
    self.attr.properties = [{func:self.setUsername, key:'username'}, {func:self.connect, key:'server'}];
    self.storage         = new StorageManager(self.attr);
    dbgc('('+self.attr.id+')ConnectionHandler initiated.');
  }
  
  // try to connect to the server
  this.connect = function(server){
    if(server == ''){ server = 'localhost'; } // in case the server is blank
    if(self.storage){ self.storage.store('server', server); }
    self.attr.server = server;
    document.ServerForm.ServerField.value = server;
    self.testConnection(server);
  }

  // Uses phonegap to assess connection status
  this.testConnection = function(url){
    self.connectionStatusMessage('connecting');
    // let's make sure we have correct protocols for remote calls
    if(url.startsWith('http://') || url.startsWith('https://')){
      dbgc('('+self.attr.id+')URL: "'+url+'" has a compatible protocol.');
    } else {
      // let's assume it's a http connection
      dbgc('('+self.attr.id+')Adding http protocol prefix to "'+url+'" URL.');
      url = 'http://'+url;
      if(self.storage){ self.storage.store('server', url); }
      document.ServerForm.ServerField.value = url;
    }
    if(navigator.network){ // if we have access to navigator.network
      dbgc('('+self.attr.id+')Got networking, testing connection to: '+url);
      navigator.network.isReachable(url, self.reachableCallback);
    } else {
      dbgc('('+self.attr.id+')Got no networking.');
      self.connectionStatusMessage('disconnected');
    }
  }
  
  this.reachableCallback = function(reachability){
    // There is no consistency on the format of reachability
    var networkState = reachability.code || reachability;

    var states = {};
    states[NetworkStatus.NOT_REACHABLE]                      = 'No network connection';
    states[NetworkStatus.REACHABLE_VIA_CARRIER_DATA_NETWORK] = 'Carrier data connection';
    states[NetworkStatus.REACHABLE_VIA_WIFI_NETWORK]         = 'WiFi connection';

    dbgc('('+self.attr.id+')Connection type: ' + states[networkState]);

    if(networkState == NetworkStatus.REACHABLE_VIA_CARRIER_DATA_NETWORK || networkState == NetworkStatus.REACHABLE_VIA_WIFI_NETWORK){
      self.connectionStatusMessage('online');
      // let the faye client try to connect to the server
      self.faye.connect(self.attr.server);
    } else {
      self.connectionStatusMessage('disconnected');
    }
  }
    
  // set and store the username
  this.setUsername = function(username){
    if(username == ''){ username = 'funkmaster'; } // in case the username is blank
    if(self.storage){ self.storage.store('username', username); }
    document.SettingsForm.UsernameField.value = username;
  }

  // create connection status messages
  this.connectionStatusMessage = function(connection_status){
    var msg = { tag:'connection' };
    msg.expire_at = Date.now() + 1000 * 10; // msg expires after 10 seconds
    
    var msg_content = 'no content';

    if(connection_status == 'connecting'){
      msg_content   = 'Connecting. \nTrying to establish connection to: "'+ self.attr.server +'"';
    } else if(connection_status == 'disconnected'){
      msg_content   = 'Disconnected. \nNo connection to "'+ self.attr.server +'" available.';
    } else if(connection_status == 'online'){
      msg_content   = 'Online. \nGot a connection to "'+ self.attr.server +'".';
    }
    
    msg.content = msg_content;
    if(!self.cache.server_status){ self.cache['server_status'] = x$('#server_status').first();}
    self.cache.server_status.innerText = 'server( '+connection_status+' ):';
    dbgc('('+self.attr.id+')Connection status: '+connection_status);
    
    self.messageScroller.messageHandler.createMessage(msg);
  }

  this.init(args);
}



/*
 *  Faye client class
 *
 *  wrapper class around the faye 'browser' client 
 * */
function FayeClient(args){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }
  var self = this;

  this.init = function(a){
    self.attr      = a;
    // we cache DOM elements in here
    self.cache = {};
    dbgc('('+self.attr.id+')FayeClient initiated.');
  }

  /* 
   *  Publish a message
   *
   *  msg: {content:'...'}
   * */
  this.publishMessage = function(msg){
    if(self.client){
      self.client.publish('/messages', msg);
    }
  }

  // message callback
  this.messageCallback = function(msg){
    dbgc('FAYE MSG: '+msg.content);
  }

  /*
   *  Try connecting to a specified server url
   * */
  this.connect = function(url){
    self.loadConfigJSON(url);  
  }

  /* 
   *  Request faye config information from server 
   * */
  this.loadConfigJSON = function(server){
    dbgc('('+self.attr.id+')Trying to get config.json from: '+server);
    
    if(!self.cache.xhr){ self.cache['xhr'] = x$('#xhr');}
    self.cache.xhr.xhr(server + '/config.json'
      ,{callback: function(){ 
        var config = JSON.parse(this.responseText);
        // if the server is not specified in the config file
        if(!config.server){
          // remove the port number
          var rex = new RegExp('(.*?)(:\\d+)');
          var match = rex.exec(server);
          if(match){ if(match[1]){ config.server = match[1]; } }
        }
        dbgc('('+self.attr.id+')config.server: '+config.server+', config.port: '+config.port);
        // seems as if we're set to finally setup bayeux handlers
        if(config.port && config.server){
          self.setupBayeuxHandlers(config);
        } else {
          dbgc('('+self.attr.id+')Configuration incomplete!');
        }
      }
      ,async:true
    });
  }

  /* 
   *  All faye/pubsub associated event listeners are setup here (should!)
   * */
  this.setupBayeuxHandlers = function(config){
    if(!self.client){
      self.client = new Faye.Client(config.server + ':' + config.port + '/faye', {
        timeout: 120
      });

      if(self.client){
        dbgc('('+self.attr.id+')FayeClient connected!');
        // subscribe to incoming messages
        self.client.subscribe('/messages', function(msg){
          self.messageCallback(msg);   
        });
    
        // subscribe to incoming data
        self.client.subscribe('/data', function(msg){
          self.messageCallback(msg);   
        });

        // let's send a welcome message
        self.publishMessage({content: 'Hello!'}); 
      } else {
        dbgc('('+self.attr.id+')Got no connected FayeClient! Can not subscribe to channels.');
      }
    }
  } 
  

  /*
   *  Returns a bool indicating whether or not we're connected
   * */
  this.connected = function(){
    if(self.client){
      return true;
    } else {
      return false;
    }
  }

  this.init(args);
}


/* 
 *  Modify some basic JS Object prototypes
 * */
var modifyPrototypes = function(){
  // this is allways NTH
  String.prototype.startsWith = function(str){
    return (this.match("^"+str)==str);
  }
}

/*
 *  Helper functions (should go into a helper class)
 * */
var roundNumber = function(num){
  var dec = 3;
  var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
  return result;
}

/*
 *  Returns a date in Datetime format, optionally of 'now'
 * */
var datetime = function(epoch_date){
  var new_date = new Date(epoch_date);
  return new_date;
}

/*
 *  DEBUG METHODS
 * */

// if debug is enabled, it will issue an alert
var dbg = function(s){
  if(debug){ alert(s); }
}

// if debug is enabled, it will log to console
var dbgc = function(s){
  if(debug){ console.log('DEBUG: '+s); }
}

// output some infos
var dbgInfos = function(){
  console.log('window.innerWidth: '+window.innerWidth);
  console.log('window.innerHeight: '+window.innerHeight);
}

// debug method that shows all entries in the persistent storage
var dba = function(){
  if(DB){
    DB.all('console.log(r)');
  } else {
    alert('NO DB!');
  }
}

// END OF DEBUG METHODS

// flags
var debug = true;
var scratchbox = false; // no accelerometer
var landscape = true; // whether or not we are in landscape view

// kind of not pretty, but works
var MessageListeners;
// for access from the console
var dancer;
var DB;

// create a lawnchair db object, need this for the interfaces
if(window.openDatabase){ 
  DB = new Lawnchair({ table:'dancer', adaptor:'webkit' }); 
}
// if(window.localStorage){ DB = new Lawnchair({ adaptor:'dom' }); }
if(!DB){ 
  alert('No Database present!'); 
} else {
  dba();
}

// start
x$(window).load(function(e){
  // modify basic JS object prototypes
  modifyPrototypes();
  // output some debug infos
  dbgInfos();
  // init our dancer client
  dancer = new Dancer();
});
