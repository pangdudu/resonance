function Dancer(){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }

  var self = this;

  this.init = function(){
    self.attr = {id:'dancer'};
    MessageListeners = [];
    self.attr.defaults   = [];
    self.attr.properties = [];
    self.storage         = new StorageManager(self.attr);
    self.addEventListeners();
    self.initInterface();
  }
  
  this.initInterface = function(){
    // Info tab
    self.dance_detector = new DanceDetector({id:'dance_detector'});
    self.message_scroller = new MessageScroller({id:'message_scroller', container:'#messages', message_tag:'all'});
    // Feedback tab
    self.current_bpm_slider = new Slider({id:'current_bpm', scale_min:64, scale_max:196});
    self.desired_bpm_slider = new Slider({id:'desired_bpm', scale_min:64, scale_max:196});
    self.track_manager = new TrackManager({id:'track_manager'});
    // Settings tab
    self.connection_handler = new ConnectionHandler({id:'connection_handler'})
    self.switchTabs(0,1,2);
  }

  this.addEventListeners = function(){
    // need this, so touchmoves aren't interpreted
	  document.addEventListener("touchmove", self.preventBehavior, false);
    
    /*
     *  Menu buttons
     * */
    x$('#btn_info').click(function(e){
      self.switchTabs(0,1,2);   // bring info tab to the front
    });
    x$('#btn_feedback').click(function(e){
      self.switchTabs(-1,0,1);  // bring feedback tab to the front
    });
    x$('#btn_settings').click(function(e){
      self.switchTabs(-2,-1,0); // bring settings tab to the front
    });
    
    /*
     *  BPM slider events in the 'Feedback' tab 
     *  (legacy events work in the sim, might use touch equivalents on phone)
     * */
    x$('#desired_bpm .knob').on('mousedown', function(e){
      self.desired_bpm_slider.activate();
    });    
    x$('#feedback').on('mouseup', function(e){
      self.desired_bpm_slider.deactivate();
    });
    // move should maybe be attached to the parent div
    x$('#feedback').on('mousemove', function(e){
      self.desired_bpm_slider.movement(e);
    });
    /*
     *  'Like this track' button in the 'Feedback' tab
     * */
    x$('#btn_like').click(function(e){
      emile(x$('#btn_like').first(), 'background: #333; color: #ffde00'
        ,{duration: 150, after: function(){
          emile(x$('#btn_like').first(), 'background: #444; color: #f32ccf', {duration: 150});
        }
      });
      self.track_manager.like();
    });
    /*
     *  'Save settings' button in the 'Settings' tab
     * */
    x$('#btn_save').click(function(e){
      emile(x$('#btn_save').first(), 'background: #333; color: #ffde00'
        ,{duration: 150, after: function(){
          emile(x$('#btn_save').first(), 'background: #444; color: #f32ccf', {duration: 150});
        }
      });
      self.connection_handler.setUsername(document.SettingsForm.UsernameField.value);
    });
    /*
     *  'Connect' button in the 'Settings' tab
     * */
    x$('#btn_connect').click(function(e){
      emile(x$('#btn_connect').first(), 'background: #333; color: #ffde00'
        ,{duration: 150, after: function(){
          emile(x$('#btn_connect').first(), 'background: #444; color: #f32ccf', {duration: 150});
        }
      });
      self.connection_handler.connect(document.ServerForm.ServerField.value);
    });
  }

  /*
   *  Switch tab to another tab
   * */
  this.switchTabs = function(info,feedback,settings){
    var transit_duration = 300;
    var current_width = window.innerWidth;
    emile(x$('#info.tab').first(), 'left: '+(info*current_width)+'px'
        , {duration: transit_duration
        , after: function(){
          x$('#info.tab').setStyle('display','block'); // first time we start tabs are hidden
        }
    });
    emile(x$('#feedback.tab').first(), 'left: '+(feedback*current_width)+'px'
        , {duration: transit_duration
        , after: function(){
          x$('#feedback.tab').setStyle('display','block'); // first time we start tabs are hidden
        }
    });
    emile(x$('#settings.tab').first(), 'left: '+(settings*current_width)+'px'
        , {duration: transit_duration
        , after: function(){
          x$('#settings.tab').setStyle('display','block'); // first time we start tabs are hidden
        }
    });
    // highlight selected button
    if(info == 0){ emile(x$('#btn_info').first(), 'background: #333; color: #ffde00', {duration: 150});
    } else { emile(x$('#btn_info').first(), 'background: #444; color: #f32ccf', {duration: 150}); }
    if(feedback == 0){ emile(x$('#btn_feedback').first(), 'background: #333; color: #ffde00', {duration: 150});
    } else { emile(x$('#btn_feedback').first(), 'background: #444; color: #f32ccf', {duration: 150}); }
    if(settings == 0){ emile(x$('#btn_settings').first(), 'background: #333; color: #ffde00', {duration: 150});
    } else { emile(x$('#btn_settings').first(), 'background: #444; color: #f32ccf', {duration: 150}); }
  }

  /* 
   * phonegap feedback functions
   */
  this.beep = function(){
	  navigator.notification.beep(2);
  }
	
  this.vibrate = function(){
  	navigator.notification.vibrate(250);
  }
	
  this.blink = function(){
    navigator.notification.blink(10, 0xffffff);
  }

  this.preventBehavior = function(e){ 
    e.preventDefault(); 
  }
  
  this.init();
};



/*
 *  Connection Handler class
 * */
function ConnectionHandler(args){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }
  var self = this;

  this.init = function(a){
    self.attr     = a;
    self.messageScroller = new MessageScroller({id:'connection_message_scroller'
        ,container:'#connection_status', message_tag:'connection'});
    self.attr.defaults   = [{key:'username', value:'funkmaster'}, {key:'server', value:'localhost'}];
    self.attr.properties = [{func:self.setUsername, key:'username'}, {func:self.connect, key:'server'}];
    self.storage         = new StorageManager(self.attr);
  }
  
  // try to connect to the server
  this.connect = function(server){
    if(server == ''){ server = 'localhost'; } // in case the server is blank
    if(self.storage){ self.storage.store('server', server); }
    self.attr.server = server;
    document.ServerForm.ServerField.value = server;
    self.testConnection(server);
  }

  // Uses phonegap to assess connection status
  this.testConnection = function(hostname){
    self.connectionStatusMessage('connecting');
    if(navigator.network){ // if we have no navigator.network (in the simulator)
      navigator.network.isReachable(hostname, self.reachableCallback);
    } else {
      self.connectionStatusMessage('disconnected');
    }
  }
  
  this.reachableCallback = function(reachability){
    // There is no consistency on the format of reachability
    var networkState = reachability.code || reachability;

    var states = {};
    states[NetworkStatus.NOT_REACHABLE]                      = 'No network connection';
    states[NetworkStatus.REACHABLE_VIA_CARRIER_DATA_NETWORK] = 'Carrier data connection';
    states[NetworkStatus.REACHABLE_VIA_WIFI_NETWORK]         = 'WiFi connection';

    // alert('Connection type: ' + states[networkState]);

    if(networkState == NetworkStatus.REACHABLE_VIA_CARRIER_DATA_NETWORK || networkState == NetworkStatus.REACHABLE_VIA_WIFI_NETWORK){
      self.connectionStatusMessage('online');
    }
  }
    
  // set and store the username
  this.setUsername = function(username){
    if(username == ''){ username = 'funkmaster'; } // in case the username is blank
    if(self.storage){ self.storage.store('username', username); }
    document.SettingsForm.UsernameField.value = username;
  }

  // create connection status messages
  this.connectionStatusMessage = function(connection_status){
    var msg = { tag:'connection' };
    msg.expire_at = Date.now() + 1000 * 10; // msg expires after 10 seconds
    
    if(connection_status == 'connecting'){
      msg.content   = 'Connecting. \nTrying to establish connection to: "'+ self.attr.server +'"';
    } else if(connection_status == 'disconnected'){
      msg.content   = 'Disconnected. \nNo connection to "'+ self.attr.server +'" available.';
    } else if(connection_status == 'online'){
      msg.content   = 'Online. \nGot a connection to "'+ self.attr.server +'".';
    }
    
    self.messageScroller.messageHandler.createMessage(msg);
  }

  this.init(args);
}



/*
 *  Storage Manager class
 * */
function StorageManager(args){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }
  var self = this;

  this.init = function(a){
    self.attr     = a;
    self.check(self.attr.defaults);
    self.load(self.attr.properties);
  }

  // store a property in the persistent storage
  this.store = function(key, value){
    if(DB){
      var identifier = self.attr.id + '_persistent_properties';
      DB.find('r.identifier == "'+identifier+'"', function(r){
        r[key] = value;
        DB.save(r);
      });
    }
  }
  
  // check if the persistent storage for this object is created or do so
  this.check = function(defaults){
    if(DB){
      var identifier = self.attr.id + '_persistent_properties';
      // will be used, if storage hasn't been created
      var storage     = { identifier:identifier, created_at:Date.now() };
      for(i in defaults){
        var def = defaults[i];
        storage[def.key] = def.value; 
      }
      var found       = false;
      // TODO: i'm handling the find method, as if it where synchronous, it's not!
      DB.find('r.identifier == "'+identifier+'"', function(r){ found = true; });
      // if no storage was found, create a new one
      if(!found){ 
        DB.save(storage);
      }
    }
  }

  // load properties from the persistent store and update the interface
  this.load = function(properties){
    if(DB){
      var identifier = self.attr.id + '_persistent_properties';
      DB.find('r.identifier == "'+identifier+'"', function(r){
        for(i in properties){
          var property = properties[i];
          if(r[property.key]){ property.func(r[property.key]); }
        }
      });
    } else { // if we have no db, load defaults instead
      for(i in properties){
        var property = properties[i];
        if(self.attr.defaults[property.key]){ property.func(self.attr.defaults[property.key]); }
      }
    }
  }

  this.init(args);
}



/*
 *  MessageHandler class
 * */
function MessageHandler(args){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }
  var self = this;

  this.init = function(a){
    self.attr     = a;
    self.attr.id  = self.attr.creator_id+'_message_handler' 
    self.attr.default_lifetime = 1000 * 60 * 30; // message expires after 30 minutes
    self.messages = []; // messages are temporarly stored here
    self.reloadMessages();
    MessageListeners.push(self); // gets calles when there is a new message
  }

  /*
   *  Trigger listener functions for new messages
   * */
  this.newMessage = function(){
    for(i in MessageListeners){ 
      var listener = MessageListeners[i];
      if(listener.reloadMessages){ listener.reloadMessages(); }
    }
  }

  /* 
   * create a new message and store it
   *
   * triggers: 'reloadMessages', which updates the messages array
   *
   * msg should have: {content:x}
   * */
  this.createMessage = function(msg){
    msg.type         = 'message';
    msg.created_by   = self.attr.creator_id;
    msg.created_at   = Date.now();
    if(!msg.expire_at){ msg.expire_at = Date.now() + self.attr.default_lifetime; }
    if(!msg.tag){ msg.tag = 'info'; }
    if(DB){
      DB.save(msg);
    } else {
      if(self.filterByTag(msg)){ self.messages.push(msg); }
    }
    self.newMessage(); // trigger a new message event
  }

  /*
   *  reload messages from persistent storage and delete expired messages
   * */
  this.reloadMessages = function(){
    if(DB){
      self.messages = [];
      DB.find('r.type == "message"', function(r){
        // check if the message has expired
        var expired = (r.expire_at <= Date.now());
        if (expired){
          DB.remove(r.key);
        } else {
          if(self.filterByTag(r)){ self.messages.push(r); }
        }
      });
    } else {
      var new_messages = [];
        for(i in self.messages){
        var msg = self.messages[i];
        // check if the message has expired
        var expired = (msg.expire_at <= Date.now());
        if(!expired){
          if(self.filterByTag(msg)){ new_messages.push(msg); }
        }
      }
      self.messages = new_messages;
    }
  }

  // Filters messages based on their tag (TODO: should be tags)
  this.filterByTag = function(msg){
    var pass = false;
    if(self.attr.message_tag == 'all'){
      pass = true;
    } else if(msg.tag == self.attr.message_tag){
      pass = true;
    }
    return pass;
  }

  this.init(args);
}



/*
 *  Dance detector and activity display class
 * */
function DanceDetector(args){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }
  var self = this;

  this.init = function(a){
    self.attr     = a;
    self.attr.max_height     = 240; // 240px is max
    self.attr.frequency      = 100; // try to update accelerometer with 100Hz
    self.attr.delta_interval = 1 * self.attr.frequency; // compute delta over 10 seconds
    self.attr.dance_lim      = 50; // delta sum value where 'dancing' starts
    self.deltas = [];
    self.delta_sum = 0;
    self.dancing = false; // current dance state
    self.messageHandler = new MessageHandler({creator_id:self.attr.id, message_tag:'dance'});
    self.watchAccel();
  }

  // watch for accelerometer changes
  this.watchAccel = function(){
  	var suc = function(a){
      // reduce decimal places to 3
      var x = roundNumber(a.x);
  		var y = roundNumber(a.y);
  		var z = roundNumber(a.z);
      // _rel are values from 0 (-2) to 1 (+2)
      var x_height = roundNumber(self.attr.max_height * Math.abs(x/2));
      var y_height = roundNumber(self.attr.max_height * Math.abs(y/2));
      var z_height = roundNumber(self.attr.max_height * Math.abs(z/2));
      // let's animate the bars
      emile(x$('#barx').first(), 'height: '+ x_height +'px', {duration: 100});
      emile(x$('#bary').first(), 'height: '+ y_height +'px', {duration: 100});
      emile(x$('#barz').first(), 'height: '+ z_height +'px', {duration: 100});
      // display the text
      x$('#delta_length').first().innerText = self.deltas.length;
      if(self.delta_sum > self.attr.dance_lim){
        x$('#delta').first().innerText = 'DANCING!!!';
        self.danceActivity(true);
      } else { 
        x$('#delta').first().innerText = self.delta_sum;
        self.danceActivity(false);
      }
      // compute new delta
      self.delta({x:x, y:y, z:z, delta:0});
  	};
    var fail = function(){};
  	var opt = { frequency:self.attr.frequency };
  	
    timer = navigator.accelerometer.watchAcceleration(suc,fail,opt);
  }
  
  /*
   *  Computes the summed delta over a specified time interval
   *
   *  v: {x:double, y:double, z:double, delta:double}
   *  deltas: [v1,v2,...vn]
   * */
  this.delta = function(v){
    // remove top elements to keep the interval in bound
    while(self.deltas.length > self.attr.delta_interval-1){
      var old = self.deltas.shift();
      // delete old deltas
      self.delta_sum = self.delta_sum - old['delta'];
    }
    // compute new delta value
    if(self.deltas.length > 0){
      // get last known values
      var last = self.deltas[self.deltas.length-1];
      // compute new delta
      v['delta'] = Math.abs(last['x'] - v['x']) + Math.abs(last['y'] - v['y']) + Math.abs(last['z'] - v['z']);
      // add new delta to delta_sum
      self.delta_sum = roundNumber(self.delta_sum + v['delta']);
    }
    // add the value to the deltas array
    self.deltas.push(v);
  }

  /*
   *  Manages current dance activity state
   * */
  this.danceActivity = function(dancing){
    // if their is an activity change
    if(dancing != self.dancing){
      if(dancing){
        self.danceInformationMessage('dancing');
      } else {
        self.danceInformationMessage('notdancing');
      }
      self.dancing = dancing;
    }
  }
  
  // create dance information messages
  this.danceInformationMessage = function(dance_info){
    var msg = { tag:'dance' };
    msg.expire_at = Date.now() + 1000 * 1; // msg expires after 1 seconds
    
    if(dance_info == 'dancing'){
      msg.content = 'You started dancing!';
    } else if(dance_info == 'notdancing'){
      msg.content = 'You are not dancing ...';
    } 
    
    self.messageHandler.createMessage(msg);
  }

  this.init(args);
}



/*
 *  Message scroller class
 * */
function MessageScroller(args){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }
  var self = this;

  this.init = function(a){
    self.attr     = a;
    self.attr.update_interval = 10000; // updates text every 10 seconds 
    self.messageHandler = new MessageHandler({creator_id:self.attr.id, message_tag:self.attr.message_tag}); // handles the message data part
    self.startTimer(self.attr.update_interval);
    self.updateInterface();
  }

  /* 
   *  Starts a periodic timer that updates the message field every n seconds
   * */
  this.startTimer = function(interval){
    self.attr.timer_id = setInterval(self.updateInterface, interval);
  }

  this.updateInterface = function(){
    // TODO: should be done through .getStyle('color')
    var text_color       = '#f32ccf';
    var background_color = '#222';
    var msg = self.messageHandler.messages.shift();
    // if we have a new message to display
    if(msg){
      self.messageHandler.messages.push(msg); // add msg to the message cue again
    } else {
      msg = {content:'no messages ...',created_at:Date.now()};
    }
    var msg_text = msg.content+'\n'+datetime(msg.created_at);
    // change text in the container
    emile(x$(self.attr.container + ' .text').first()
      ,'color: '+background_color
      ,{duration: 250
        ,after: function(){
          x$(self.attr.container + ' .text').first().innerText = msg_text;
          emile(x$(self.attr.container + ' .text').first(),'color: '+text_color,{duration: 250});
        }
    });
  }

  this.init(args);
}



/*
 *  Slider class
 * */
function Slider(args){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }
  var self = this;

  this.init = function(a){
    a.active      = false;
    a.knob_width  = parseInt(x$('#'+a.id+' .knob').getStyle('width'));
    a.scale_delta = a.scale_max - a.scale_min;
    self.attr     = a;
    self.attr.defaults   = [{ key:'frac', value:0.5 }];
    self.attr.properties = [{ func:self.setToFrac, key:'frac' }];
    self.storage         = new StorageManager(self.attr);
  }

  this.activate = function(){
    self.attr.active = true;
  }
  
  this.deactivate = function(){
    self.attr.active = false;
    self.storage.store('frac', self.attr.frac);
  }

  this.movement = function(e){
    if(self.attr.active){
      var width  = parseInt(x$('#'+ self.attr.id + ' .slider').getStyle('width'));
      var kw2    = self.attr.knob_width/2;
      var delta  = (window.innerWidth - width)/2 + kw2;
      var newpos = e.pageX - delta;
      // check if the new position is within bounds
      if( newpos >= (-1*kw2) && newpos <= (width - kw2) ){
        self.attr.frac = roundNumber( (newpos + kw2)/width );
        self.updateInterface(newpos);
      }
    }
  }

  // set to a fraction of the sliders range
  this.setToFrac = function(f){
    var width  = parseInt(x$('#'+ self.attr.id + ' .slider').getStyle('width'));
    var newpos = f * width;
    self.attr.frac = f;
    self.updateInterface(newpos);
  }

  // set to a specific BPM count within the sliders range
  this.setToBPM = function(bpm){
    if(bpm >= self.attr.scale_min && bpm <= self.attr.scale_max){
      var f      = roundNumber((bpm-self.attr.scale_min)/self.attr.scale_delta);
      var width  = parseInt(x$('#'+ self.attr.id + ' .slider').getStyle('width'));
      var newpos = f * width;
      self.attr.frac = f;
      self.updateInterface(newpos);
    }
  }
  
  this.updateInterface = function(newpos){
    // move the knob
    emile(x$('#' + self.attr.id + ' .knob').first(), 'left: '+ newpos +'px', {duration: 25});
    // update the slider text
    var text = parseInt(self.attr.scale_min + self.attr.scale_delta * self.attr.frac);
    x$('#' + self.attr.id + ' .text').first().innerText = text;
  }

  this.init(args);
}



/*
 *  Track Manager class
 *
 *  handles track likes and displays currently played tracks
 * */
function TrackManager(args){
  if (! (this instanceof arguments.callee)){
    return new arguments.callee(arguments);
  }
  var self = this;

  this.init = function(a){
    self.attr     = a;
    self.messageScroller = new MessageScroller({id:'track_message_scroller'
        ,container:'#track_information', message_tag:'track'});
    self.attr.defaults   = [];
    self.attr.properties = [];
    self.storage         = new StorageManager(self.attr);
    self.current_tracks = []; // currently playing tracks
    self.trackInformationMessage('current');
  }
  
  // 'like' the currently playing track
  this.like = function(){
    self.trackInformationMessage('like');
  }

  // create track information messages
  this.trackInformationMessage = function(track_info){
    var msg = { tag:'track' };
    msg.expire_at = Date.now() + 1000 * 10; // msg expires after 10 seconds
    // concat track titles to one string
    var tracks = '';
    for(i in self.current_tracks){ tracks = tracks +'"'+ self.current_tracks[i] +'"\n'; }
    if(tracks == ''){ tracks = '"no track"'; }
    
    if(track_info == 'current'){
      msg.content   = 'Currently playing: ' + tracks;
    } else if(track_info == 'like'){
      msg.content   = 'You like: ' + tracks;
    } 
    
    self.messageScroller.messageHandler.createMessage(msg);
  }

  this.init(args);
}



/*
 *  Helper functions (should go into a helper class)
 * */
var roundNumber = function(num){
  var dec = 3;
  var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
  return result;
}

/*
 *  Returns a date in Datetime format, optionally of 'now'
 * */
var datetime = function(epoch_date){
  var new_date = new Date(epoch_date);
  return new_date;
}

// if debug is enabled, it will issue an alert
var dbg = function(s){
  if(debug){ alert(s); }
}

// debug method that shows all entries in the persistent storage
var dba = function(){
  if(DB){
    DB.all('console.log(r)');
  } else {
    alert('NO DB!');
  }
}

// kind of not pretty, but works
var MessageListeners;
// for access from the console
var dancer;
var DB;
// create a lawnchair db object, need this for the interfaces
// if(window.openDatabase){ DB = new Lawnchair({ table:'dancer', adaptor:'webkit' }); }
if(window.localStorage){ 
  DB = new Lawnchair({ adaptor:'dom' });
}
if(!DB){ alert('No Database present!'); }

// debug flag
var debug = false;

// start
x$(window).load(function(e){
  // init our dancer client
  dancer = new Dancer();
});
