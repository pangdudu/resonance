#ifndef RESONANCE_H 
#define RESONANCE_H

#include "VdjPlugin.h"
// FILE I/O
#include <iostream>
#include <fstream>

#define NO_SYNC 0
#define SYNC    1

//////////////////////////////////////////////////////////////////////////
// Class definition
//////////////////////////////////////////////////////////////////////////
class Resonanceplugin : public IVdjPlugin
{
public:
       HRESULT VDJ_API OnLoad();
       HRESULT VDJ_API OnGetPluginInfo(TVdjPluginInfo *infos);
       ULONG   VDJ_API Release();
	   void ResetQres();
	   BOOL QueryString(const std::string &deck, const std::string &verb);
	   BOOL QueryInteger(const std::string &deck, const std::string &verb);
	   // TIMER MAGIC
	   void QueueTimerHandler();
       static void CALLBACK TimerProc(void* lpParam, BOOLEAN TimerOrWaitFired);
	   // CALL TO RESONANCE API
	   void CallResonanceAPI();
private:
	struct TVdjQueryResult qRes;
	// query result, will never be larger than 255 byte
	char QueryResult[255];
	// api server's address for VDJ api
	char *APIServer;
	// api channel handle (like 'Berghain 1st Floor', etc.)
	char *APIChannel;
	// timer and loging
	int TimerMutex;
	FILE * pFile;
};

#endif // RESONANCE_H

