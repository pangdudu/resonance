#include "ResonancePlugin.h"
#include "wininet.h"
#include <stdio.h>
#include <string>

using namespace std;

//////////////////////////////////////////////////////////////////////////
// Initialization
//////////////////////////////////////////////////////////////////////////

HRESULT __stdcall Resonanceplugin::OnLoad()
{   
	// TODO: api config should come from a config file, ...
	// the api server we want to call
	APIServer = "http://10.0.0.11:8000/api";
	// our channel handle
	APIChannel = "DJ Ruby";

	FILE * pFile; pFile = fopen ("C:\\resonance_onload.txt","a");
	if (pFile!=NULL) { fputs ("resonance onload BEFORE TIMER\n",pFile); fclose (pFile); }

	// TIMER MAGIC
	TimerMutex = 0;
	HANDLE hTimer = NULL;
	BOOL success = CreateTimerQueueTimer( &hTimer, NULL, (WAITORTIMERCALLBACK)TimerProc, this, 1000, 2000, 0 );
	// END OF TIMER MAGIC

	pFile = fopen ("C:\\resonance_onload.txt","a");
	if (pFile!=NULL) { fputs ("resonance onload AFTER TIMER\n",pFile); fclose (pFile); }

	return S_OK;
}

// reset the query result values
void Resonanceplugin::ResetQres()
{
	qRes.type = 0;
	qRes.vint = 0;
	qRes.vfloat = 0.0f;
	qRes.string = NULL;
	qRes.flag = 0;
}

// query the vdj api for a string and write the result into the QueryResult buffer
BOOL Resonanceplugin::QueryString(const string &deck, const string &verb)
{
	// clean up after prior queries
	ResetQres();
	HRESULT hr;
				
	// build our new query string
	string query = "";
	query += deck; query += " "; query += verb;

	// create a char* from the string
	char *q;
	q = new char[query.length() + 1];
	strcpy(q, query.c_str());

	hr = GetInfo(q, &qRes);
	// clean up the char* 
	delete [] q;
	// act according to the query's success
	if(qRes.string!=NULL) {
		wsprintf(QueryResult, qRes.string); // successful, write the result into our buffer
		return true;
	} else {
		wsprintf(QueryResult, ""); // fallback, not successful
		return false;
	}
}

// query the vdj api for an integer and write the result into the QueryResult buffer
BOOL Resonanceplugin::QueryInteger(const string &deck, const string &verb)
{
	// clean up after prior queries
	ResetQres();
	HRESULT hr;
				
	// build our new query string
	string query = "";
	query += deck; query += " "; query += verb;

	// create a char* from the string
	char * q;
	q = new char[query.length() + 1];
	strcpy(q, query.c_str());

	hr = GetInfo(q, &qRes);
	// clean up the char* 
	delete [] q;
	// act according to the query's success
	if(qRes.vint != 0) {
		wsprintf(QueryResult, "%i", qRes.vint); // successful, write the result into our buffer
		return true;
	} else {
		wsprintf(QueryResult, "0"); // fallback, if not successful
		return false;
	}
}

// make an api call
void Resonanceplugin::CallResonanceAPI()
{
	// build a timestamp
	SYSTEMTIME st;
	GetSystemTime(&st);
	char *timestamp = new char[64];
	sprintf(timestamp, "%04d%02d%02d%02d%02d%02d%03d", st.wYear, st.wMonth,
		st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);

	// make vdj api calls and build JSON
	string json = "{'VDJInfo':{";
	
	// GLOBAL
	json += "'timestamp':'"; json += timestamp; json += "'";
	json += ",'channel':'"; json += APIChannel; json += "'";
	if(QueryString("", "get version")) {
		json += ",'version':'"; json += QueryResult; json += "'";
	}
	if(QueryInteger("", "get activedeck")) {
		json += ",'activeDeck':'"; json += QueryResult; json += "'";
	}
	// FOR EACH DECK
	for(int i = 1; i < 5; i++) {
		char *deck = new char[8];
		sprintf(deck, "deck %i", i);
		// OPEN DECK i
		json += ",'"; 
		json += deck;
		json += "':{"; 
		
		// OPEN DECK SONG
		json += "'song':{";
		// BPM should always be there once a song is loaded, no ',' problems
		if(QueryString(deck, "get loaded_song 'bpm'")) {
			json += "'bpm':'"; json += QueryResult; json += "'";
		}
		if(QueryString(deck, "get loaded_song 'album'")) {
			json += ",'album':'"; json += QueryResult; json += "'";
		}
		if(QueryString(deck, "get loaded_song 'author'")) {
			json += ",'author':'"; json += QueryResult; json += "'";
		}
		if(QueryString(deck, "get loaded_song 'title'")) {
			json += ",'title':'"; json += QueryResult; json += "'";
		}
		if(QueryString(deck, "get loaded_song 'key'")) {
			json += ",'key':'"; json += QueryResult; json += "'";
		}
		json += "}"; 
		// CLOSE DECK SONG
		
		// OPEN DECK TIME
		json += ",'time':{"; 
		// TOTALTIME should always be there once a song is loaded, no ',' problems
		if(QueryInteger(deck, "get totaltime_min")) {
			json += "'totaltimeMin':'"; json += QueryResult; json += "'";
		}
		if(QueryInteger(deck, "get totaltime_sec")) {
			json += ",'totaltimeSec':'"; json += QueryResult; json += "'";
		}
		if(QueryInteger(deck, "get time_min")) {
			json += ",'timeMin':'"; json += QueryResult; json += "'";
		}
		if(QueryInteger(deck, "get time_sec")) {
			json += ",'timeSec':'"; json += QueryResult; json += "'";
		}
		json += "}"; 
		// CLOSE DECK TIME
	
		if(QueryInteger(deck, "get volume")) {
			json += ",'volume':'"; json += QueryResult; json += "'";
		}
		json += "}"; 
		// CLOSE DECK i
	}
	// END OF FOR EACH DECK

	// finally, CLOSE VDJINFO
	json += "}}";

	// LOG API SERVER ADDRESS
	pFile = fopen ("C:\\resonance_timer_current.txt","a");
	if (pFile!=NULL) {
		fputs(json.c_str(), pFile); fputs ("\n", pFile);
		fputs("WILL NOW CALL THE API AT: ", pFile);
		fputs(APIServer, pFile); fputs ("\n", pFile);
		fclose (pFile);
	}

	string apiCall = APIServer;
	apiCall += "?json=";
	apiCall += json;
	// MS magic internet function wants LPCSTR, so we build it one
	LPCSTR lpcApiCall = apiCall.c_str();
	// need a session handle to make the connection
	HINTERNET hSession = InternetOpen("API CALL", 0, NULL, NULL, 0);
	HINTERNET hOpenUrl = InternetOpenUrl(hSession, lpcApiCall, NULL, 0, 1, 1);
	
	// LOG THE API CALL STATUS
	pFile = fopen ("C:\\resonance_timer_current.txt","a");
	if (pFile!=NULL) {
		if( hOpenUrl == NULL) {
			DWORD rc = GetLastError ();
			if (rc == ERROR_INTERNET_NAME_NOT_RESOLVED) {
				fputs("API STATUS: Invalid url.\n", pFile);
			} else {
				fputs("API STATUS: Some other error with InternetOpenUrl.\n", pFile);
			}
		} else {
			fputs("API STATUS: API call successful.\n", pFile);
		}
		fclose (pFile);
	}

	// clean up the handles
	InternetCloseHandle(hOpenUrl);
	InternetCloseHandle(hSession);
}

void Resonanceplugin::QueueTimerHandler() // called every elTime milliseconds
{
	if(TimerMutex == 0) {
		// API mutex
		TimerMutex = 1;
		CallResonanceAPI();
		// reset the timer tock
		TimerMutex = 0;
	}
}

// must be static to get called by the timer, i guess
VOID CALLBACK Resonanceplugin::TimerProc(PVOID lpParam, BOOLEAN TimerOrWaitFired)
{
	Resonanceplugin* obj = (Resonanceplugin*) lpParam;
	obj->QueueTimerHandler();
}

//------------------------------------------------------------------------
HRESULT __stdcall Resonanceplugin::OnGetPluginInfo(TVdjPluginInfo *infos)
{
	infos->Author="Nick Thomas aka DJ Ruby";
	infos->PluginName="Resonance connector v1.0";
	infos->Description="A small plugin to extract useful information for the Resonance system.";
	infos->Flag=0;

	return S_OK;
}

//------------------------------------------------------------------------
ULONG __stdcall Resonanceplugin::Release()
{
	delete this;
	return 0;
}