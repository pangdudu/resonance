#include "ResonancePlugin.h"


HRESULT __stdcall DllGetClassObject(const GUID &rclsid,const GUID &riid,void** ppObject)
{
 // This is the standard DLL loader for COM object.
 // You don't need to change anything in this function.
 if(memcmp(&rclsid,&CLSID_VdjPlugin6,sizeof(GUID))!=0) return CLASS_E_CLASSNOTAVAILABLE;
 *ppObject=new Resonanceplugin();
 return NO_ERROR;
}