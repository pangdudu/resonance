require 'rubygems'
require 'eventmachine'
require 'faye'

client = Faye::Client.new('http://localhost:8000/faye')
counter = 0 

def publish client,message
  client.publish('/messages', 'content' => message)   
  puts "send message: #{message}" 
end
 
EM.run do

  client.subscribe('/messages') do |message|
    puts "received message: #{message.inspect}"
  end
  
  EventMachine.add_periodic_timer(0.1) {
    counter += 1
    publish(client,"bleep (#{counter})")
  }

end


