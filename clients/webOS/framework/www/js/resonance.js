var debug;
var $ps = new PubSub(); // very simple pub/sub implementation (local)
/* GLOBAL HELPER CLASSES */
var roundNumber = function(num,precision){
  var dec = 3;
  if(parseInt(precision) > 0){ dec = precision };
  var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
  return result;
}
/* END OF GLOBAL HELPER CLASSES */

/*
*  Main controller
* */
function Resonance() {
  if (!(this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

  // called before ST initializes the gui
  this.init = function() {
    // initial content for ST Tabs
    self.content = {
      dance: 'Dance'
      ,tracks: 'Tracks'
      ,crowd: 'Crowd'
      ,connect: 'Connect'
      ,calibrate: 'Calibrate'
      ,dev: 'Dev'
    };
    self.id = 'rda_alpha';
    // configure internal (local) pub/sub messaging
    self.subscribe();
    // initialize sensor interfaces
    self.sensor = new Sensor({ accelerometer: { frequency: 100, interval: 100 } });
    debug = self;
  };

/*
* init tab controllers (called after ST initialized the gui) 
* */
  this.initTabController = function(components) {
    // initialize tab content controller
    self.logger  = new Logger({ id: 'dev', component: components.dev, title: "Log:" });
    self.crowd  = new Logger({ id: 'crowd', component: components.crowd, title: "Crowd:" });
    self.tracks = new Tracks({ id: 'tracks', component: components.tracks, autoNuke: true });
    self.connect = new Connect({ id: 'connect', component: components.connect, logger: self.logger });
    self.dance = new Dance({ id: 'dance', component: components.dance, logger: self.logger });
  };

/*
* Configure internal pub/sub subscriptions
* */
  this.subscribe = function() {
    // connection state handling
    $ps.subscribe('/status/connection', function(m) { 
      if(m.online) {
        self.setupBayeuxHandlers(m);
        alert('Ready to go!');
      } else if(m.faye) {
        self.log('.subscribe: Got Faye');
        // we need to inform other nodes of our presence, periodicaly
        self.networkTimer = setInterval(self.publishNetwork, 5000);
      };
    });
    // sensor data relaying
    $ps.subscribe('/sensors/accelerometer', function(m) {
      if(self.faye && m.delta) {
        m.accelerometerUpdate = true;
        // add a timestamp (... always nice)
        m.createdAt = Date.now();
        self.faye.publish(self.dataChannel, m);
      };
    });
    // send track status info to server
    $ps.subscribe('/tracks/update', function(m) {
      if(m){
        m.trackUpdate = true;
        m.createdAt = Date.now();
        self.faye.publish(self.dataChannel, m);
      }; 
    });
  };

  /* 
   *  Publishes client information to network
   *
   *  channel: /network
   *  content: sender, createdAt, infoChannel, dataChannel
   * */
  this.publishNetwork = function() {
    var info = { sender: self.id, createdAt: Date.now() };
    // add channel information
    info.infoChannel = self.infoChannel;
    info.dataChannel = self.dataChannel;
    // publish the information to network
    self.faye.publish('/network', info);
  };

  /* 
  *  The websocket/faye event listeners are setup here 
  * */
  this.setupBayeuxHandlers = function(config) {
    self.faye = new Faye.Client("http://" + config.server + ':' + config.port + '/faye', {
      timeout: 120
    });
    // build our faye id using the username
    self.id = 'rda_'+config.username;
    // send out a hello! to the system
    self.faye.publish('/global/info', { sender: self.id, content: 'Android Dancer ('+config.username+') connected!' });
    // create channel handles we want to publish to
    self.infoChannel = '/'+self.id+'/information';
    self.dataChannel = '/'+self.id+'/data';
    // publish faye availability
    $ps.publish('/status/connection', { faye: true });
    // subscribe to global info channel
    self.faye.subscribe('/global/info', function(msg) {
      self.crowd.log('(INFO) '+msg.content);
    });
    // subscribe to global track info channel
    self.faye.subscribe('/global/tracks', function(msg) {
      //console.log('SUBSCRIBE: TRACKS: '+msg.tracks);
      $ps.publish('/status/tracks', msg);
    });
  };

  this.log = function(txt) {
    self.logger.log('R'+txt);
  };

  this.init();
};
