Ext.setup({
  onReady: function() {
    var resonance = new Resonance();
    var tabpanel = new Ext.TabPanel({
      tabBar: {
        dock: 'bottom',
        layout: {
          pack: 'center'
        }
      },
      fullscreen: true,
      ui: 'dark',
      cardSwitchAnimation: {
        type: 'fade',
        cover: true
      },
      defaults: {
        scroll: 'vertical'
      },
      items: [{
        title: 'Dance',
        id: 'dance',
        iconCls: 'user',
        cls: 'card card1',
        html: resonance.content.dance
      }, {
        title: 'Tracks',
        id: 'tracks',
        iconCls: 'favorites',
        cls: 'card card2',
        html: resonance.content.tracks
      }, {
        title: 'Crowd',
        id: 'crowd',
        cls: 'card card3',
        iconCls: 'team',
        html: resonance.content.crowd
      }, {
        title: 'Connect',
        id: 'connect',
        cls: 'card card4',
        iconCls: 'settings',
        html: resonance.content.connect
      }, {
        title: 'Dev',
        id: 'dev',
        cls: 'card card2',
        iconCls: 'more',
        html: resonance.content.dev
      }]
    });
    var c = {};
    c.dance = tabpanel.items.map.dance;
    c.tracks = tabpanel.items.map.tracks;
    c.crowd = tabpanel.items.map.crowd;
    c.connect = tabpanel.items.map.connect;
    c.dev = tabpanel.items.map.dev;
    resonance.initTabController(c);
  }
});
