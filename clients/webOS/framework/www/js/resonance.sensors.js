/*
 *  Sensor interface (publishes it's data on internal pub/sub)
 * */
function Sensor(args) {
  if (!(this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

  this.init = function(args) {
    self.config = args;
    self.deltas = {};
    // if we have an accelerometer, start polling it
    if(navigator.accelerometer) {
      self.deltas.accel = { sum: 0, buffer: [], interval: self.config.accelerometer.interval, factor: 1, peak: 100 };
      self.watchAccel();
    };
  };

 /*
  *  Monitor accelerometer changes
  * */
  this.watchAccel = function() {
      
    var suc = function(a) {
      // compute new delta
      self.delta(self.deltas.accel, { x:a.x, y:a.y, z:a.z, delta: 0 });
      var data =  { x: a.x, y: a.y, z: a.z, deltaRaw: self.deltas.accel.sum };
      // this delta is normalized  (0-100) 
      data.delta = self.deltas.accel.sum * self.deltas.accel.factor;
      $ps.publish('/sensors/accelerometer', data);
  	};
    
    var fail = function() {};
  	
    var opt = { frequency: self.config.accelerometer.frequency }; // 50ms should result in 20Hz
  	
    timer = navigator.accelerometer.watchAcceleration(suc, fail, opt);
  };

  /*
   *  Computes the summed delta over a specified time interval
   *
   *  v: {x:double, y:double, z:double, delta:double}
   * */
  this.delta = function(d, v) {
    // remove top elements to keep the interval in bound
    while(d.buffer.length > d.interval-1) {
      var old = d.buffer.shift();
      // delete old deltas
      d.sum = d.sum - old.delta;
    };
    // compute new delta value
    if(d.buffer.length > 0){
      // get last known values
      var last = d.buffer[d.buffer.length-1];
      // compute new delta
      v.delta = Math.abs(last.x - v.x) + Math.abs(last.y - v.y) + Math.abs(last.z - v.z);
      // add new delta to delta sum
      d.sum = d.sum + v.delta;
      // normalize if neccessary
      self.normalize(d, v);
    };
    // add the value to the deltas array
    d.buffer.push(v);
  };

 /*
  * TODO: max peak normalisation
  * */
  this.normalize = function(d, v) {
    if(d.sum > d.peak) {
      d.peak = d.sum;
      d.factor = 100/d.sum;
    };
  };

  this.init(args);
};
