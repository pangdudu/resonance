/*
 * Simple pub/sub msg router
 * */
function PubSub(args) {
  if (!(this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  }

  var self = this;

  this.init = function(args) {
    self.cache = {};
  };
  
  this.publish = function(topic, args) {
    // x$.publish("/some/topic", { a: 'a', b: 'b'});
    if(self.cache[topic]) {
      var callbacks = self.cache[topic];
      for(var i = 0; i < callbacks.length; i++) {
        var callback = callbacks[i];
        if(!args) { args = {} };
        callback(args);
      };
    };
  };

  this.subscribe = function(topic, callback){
    // $.subscribe("/some/topic", function(a, b, c){ /* handle data */ });
    if(!self.cache[topic]){
      self.cache[topic] = [];
    }
    self.cache[topic].push(callback);
    return [topic, callback]; // Array
  };

  this.init();
};
