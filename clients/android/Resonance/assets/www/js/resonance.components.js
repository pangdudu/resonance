/*
 * 'Tracks' tab controller
 * */
function Tracks(args) {
  if (!(this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

  this.init = function(args) {
    self.id        = args.id;
    self.component = args.component;
    self.autoNuke  = args.autoNuke;
    self.status    = { dancing: false, tracks: [] };
    self.logger  = new Logger({ id: 'tracks', component: self.component, title: "Tracks:" });
    self.subscribe();
    // initialize DB
    if(window.openDatabase){ 
     self.DB = new Lawnchair({ table:'tracks', adaptor:'webkit' }); 
      if(self.DB){
        if(self.autoNuke){ 
          self.DB.nuke();
          self.log('DB NUKED!');
        };
        // build the index we'll use to find existing tracks in the db
        self.index = {};
        self.DB.each(function(r){ self.index[r.track_id] = r.key; });
        // start dance status timer (after 1500ms, each 1500ms)
        self.trackStatusTimer = setInterval(self.trackStatus, 1500);
        // start tracks update timer (after 10000ms, each 10000ms)
        self.tracksUpdateTimer = setInterval(self.updateTracks, 10000);
        self.log('DB initialized!');
      };
    };
  };

 /*
  *  Update User Interface and publish Track Infos
  * */
  this.updateTracks = function() {
    self.DB.each(function(r) {
      var activity = roundNumber(r.dancing/r.total*100,2);
      var msg = r;
      msg.activity = activity;
      msg.check = true;
      $ps.publish('/tracks/update', msg);
      self.log('('+r.title+') Activity: '+msg.activity+'%, Total: '+r.total);
    });
  };


 /*
  * Store Track/Dancing tupels for track favorite detection
  * */
  this.trackStatus = function() {
    // calculate new track status information
    if(self.status.tracks.length > 0){
      for(var i = 0; i < self.status.tracks.length; i++){
        var track    = self.status.tracks[i];
        var track_id = track.title+track.author;
        if(self.index[track_id]){
          self.DB.get(self.index[track_id], function(r) {
            //console.log('TrackTotal: '+r.total);
            r.total += 1;
            if(self.status.dancing) { r.dancing += 1 };
            self.DB.save(r, function(rr){
              //console.log('TrackStatus: Saved!');
            });
          });
        } else {
          var info       = { track_id: track_id, title: track.title, album: track.album, author: track.author, bpm: track.bpm };
          info.createdAt = Date.now();
          info.total     = 1;
          info.dancing   = 0;
          if(self.status.dancing) { info.dancing = 1 };
          self.DB.save(info, function(r) {
            self.index[track_id] = r.key;
          });
        };
      };
    };
  };

 /*
  * Configure internal pub/sub subscriptions
  * */
  this.subscribe = function() {
    // dance status (set by Dance)
    $ps.subscribe('/status/dance', function(m) {
      if(m){
        self.status.dancing = m.dancing;
      };
    });
    // track info (set by Resonance[external pub/sub])
    $ps.subscribe('/status/tracks', function(m) {
      if(m && m.tracks) {
        if(m.tracks.length > 0){ 
          //console.log('Tracks: '+m.tracks);
          self.status.tracks = m.tracks; 
        } else {
          self.status.tracks = []; 
          //console.log('Tracks: EMPTY!');
        };
      };
    });
  };

  this.log = function(txt) {
    self.logger.log(txt, 'tracks');
  };

  this.init(args);
};


/*
 * 'Dance' tab controller
 * */
function Dance(args) {
  if (!(this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

  this.init = function(args) {
    self.id        = args.id;
    self.component = args.component;
    self.logger    = args.logger;
    // gateway for dance detection
    self.gate      = 50;
    // reduce drawing by detecting changes in 1/10 steps of delta
    self.current_tenth = 0;
    // initialize interface elements
    self.initInterface();
    // subscribe to relevant channels
    self.subscribe();
    // start dance status timer
    self.danceStatusTimer = setInterval(self.danceStatus, 1000);
    self.log('.init: dance tab controller initialized for id: "'+self.id+'"');
    //debug = self;
  };

  this.danceStatus = function() {
    var status = false;
    if(self.current_tenth > self.gate/10) { status = true; };
    $ps.publish('/status/dance', { timestamp: Date.now, check: true, dancing: status });
  };

 /*
  * Configure internal pub/sub subscriptions
  * */
  this.subscribe = function() {
    $ps.subscribe('/sensors/accelerometer', function(m) { 
      // look at delta 10th for change detection
      var tenth = m.delta/10 | 0;
      if(!self.drawing && m.delta && (tenth != self.current_tenth)) {
        self.drawing = true;
        self.current_tenth = tenth;
        // colors for delta bars
        if(self.deltas) {
          self.deltas.dn.setStyle('backgroundColor', 'transparent');
          if(m.delta > 0)  { self.deltas.d1.setStyle('backgroundColor', '#00e18f'); };
          if(m.delta > 10) { self.deltas.d2.setStyle('backgroundColor', '#00e18f'); };
          if(m.delta > 20) { self.deltas.d3.setStyle('backgroundColor', '#00e18f'); };
          if(m.delta > 30) { self.deltas.d4.setStyle('backgroundColor', '#00e18f'); };
          if(m.delta > 40) { self.deltas.d5.setStyle('backgroundColor', '#00e18f'); };
          if(m.delta > 50) { self.deltas.d6.setStyle('backgroundColor', '#ffde00'); };
          if(m.delta > 60) { self.deltas.d7.setStyle('backgroundColor', '#ffde00'); };
          if(m.delta > 70) { self.deltas.d8.setStyle('backgroundColor', '#ffde00'); };
          if(m.delta > 80) { self.deltas.d9.setStyle('backgroundColor', '#ffde00'); };
          if(m.delta > 90) { self.deltas.d10.setStyle('backgroundColor','#ffde00'); };
        };
        if(self.title) {
          if(m.delta >= self.gate) { 
            self.title.html('<h1>Dancing</h1>');
          } else {
            self.title.html('<h1>Resting</h1>');
          };
        };
        self.drawing = false;
      };
    });
  };

 /*
  * Init interface elements
  * */
  this.initInterface = function() {
    self.component.update('<div class="title"><h1>Dance</h1></div><div class="container"><div class="first"></div></div>');
    self.title   = x$('div#'+self.id+' div.title');
    self.content = x$('div#'+self.id+' div.container');
    self.first   = x$('div#'+self.id+' div.container div.first');
    self.delta   = self.initDeltaInterface();
  };

  this.initDeltaInterface = function() {
    self.first.after('<div id="delta"><div id="d10"></div><div id="d9"></div><div id="d8"></div><div id="d7"></div><div id="d6"></div><div id="d5"></div><div id="d4"></div><div id="d3"></div><div id="d2"></div><div id="d1"></div></div>');
    self.deltas    = {};
    self.deltas.dn  = x$('div#delta div');
    self.deltas.d1  = x$('div#delta div#d1');
    self.deltas.d2  = x$('div#delta div#d2');
    self.deltas.d3  = x$('div#delta div#d3');
    self.deltas.d4  = x$('div#delta div#d4');
    self.deltas.d5  = x$('div#delta div#d5');
    self.deltas.d6  = x$('div#delta div#d6');
    self.deltas.d7  = x$('div#delta div#d7');
    self.deltas.d8  = x$('div#delta div#d8');
    self.deltas.d9  = x$('div#delta div#d9');
    self.deltas.d10 = x$('div#delta div#d10');
  };

  this.log = function(txt) {
    self.logger.log('DTC'+txt, 'dance');
  };

  this.init(args);
};

/*
 * 'Connect' tab controller
 * */
function Connect(args) {
  if (!(this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

  this.init = function(args) {
    self.id        = args.id;
    self.component = args.component;
    self.logger    = args.logger; 
    // initialize interface elements
    self.initInterface();   // setup html for content
    self.log('.init: connect tab controller initialized for id: "'+self.id+'"');
  };

/*
* Initiate connect sequence
* */
  this.connect = function() {
    // add a new log message
    self.url = 'http://'+self.server+':'+self.port;
      if(navigator.network) {
      self.log('.connect(phone): url: '+self.url);
      navigator.network.isReachable(self.url, self.reachableCallback);
    } else {
      self.log('.connect: Connecting to '+self.url);
      self.title.html('<h1>Browser</h1>');
      $ps.publish('/status/connection', { online: true, username: self.username, url: self.url, server: self.server, port: self.port });
    };
  };

/*
* Get's called by the navigator when checking connection status
* */
  this.reachableCallback = function(reachability){
    // There is no consistency on the format of reachability
    var networkState = reachability.code || reachability;

    var states = {};
    states[NetworkStatus.NOT_REACHABLE]                      = 'No network connection';
    states[NetworkStatus.REACHABLE_VIA_CARRIER_DATA_NETWORK] = 'Carrier data connection';
    states[NetworkStatus.REACHABLE_VIA_WIFI_NETWORK]         = 'WiFi connection';

    if(networkState == NetworkStatus.REACHABLE_VIA_CARRIER_DATA_NETWORK || networkState == NetworkStatus.REACHABLE_VIA_WIFI_NETWORK){
      self.title.html('<h1>Online</h1>');
      $ps.publish('/status/connection', { online: true, username: self.username, url: self.url, server: self.server, port: self.port });
    } else {
      self.title.html('<h1>Error</h1>');
    };
  };

/*
* Init interface elements
* */
  this.initInterface = function() {
    self.form = new Ext.form.FormPanel({
      items: [{
        xtype: 'textfield',
        id   : 'username',
        name : 'username',
        label: 'User',
        value: 'android_1'
      }, {
        xtype: 'urlfield',
        id   : 'server',
        name : 'server',
        label: 'Server',
        value: '10.0.0.15'
      }, {
        xtype: 'numberfield',
        id   : 'port',
        name : 'port',
        label: 'Port',
        value: '8000'
      }, {
        id: 'btnConnect',
        xtype: 'button',
        ui  : 'round',
        text: 'Connect'
      }]
    });
    self.component.add(self.form);
    self.component.update('<div class="title"><h1>Offline!</h1></div>');
    // the form has to be rendered into the dom before we can add something 'after' it
    self.form.on('afterrender', function() {
      self.title = x$('div#'+self.id+' div.title');
      x$('div#'+self.id+' form').after('<div class="container"><div class="first"></div></div>');
      self.content = x$('div#'+self.id+' div.container');
      self.first   = x$('div#'+self.id+' div.container div.first');
    });
    // we need to initiate the connect sequence once the button was pressed
    self.form.items.map.btnConnect.on('tap', function(btn, e) {
      // cache the 'input' dom elements
      self.username = x$('div#username input')[0].value;
      self.server   = x$('div#server input')[0].value;
      self.port     = x$('div#port input')[0].value;
      self.connect();
    });
  };

  this.log = function(txt) {
    self.logger.log('CTC'+txt, 'connect');
  };

  this.init(args);
};

/*
* Simple logger class
* */
function Logger(args) {
  if (!(this instanceof arguments.callee)) {
    return new arguments.callee(arguments);
  };

  var self = this;

  this.init = function(args) {
    self.id    = args.id;
    self.title = args.title;
    self.card  = x$('#'+self.id+' div.x-scroller');
    // setup html for content
    self.card.html('<h1>'+self.title+'</h1><div class="container"><div class="first"></div></div>');
    self.content = x$('div#'+self.id+' div.x-scroller div.container');
    self.first   = x$('div#'+self.id+' div.x-scroller div.container div.first');
    if(self.id == 'dev'){
      self.log('Logger initialized for id: "'+self.id+'"');
      // some additional debug infos
      x$(Faye.CONNECTION_TYPES).each(function(ct) { self.log('FAYE: ACT: '+ct); });
    };
  };

  this.log = function(txt, cls) {
    if(!cls) { cls = ''; }; // fallback
    self.first.after('<div class="msg log '+cls+'">'+txt+'</div>');
  };

  this.init(args);
};
