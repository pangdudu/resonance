---
<h2>RESONANCE</h2>

Howdy ho!

Resonance is a DJ Assistance Framework that I implemented as a proof-of-concept for my thesis.

It features a node.js backend and clients for android, n900, webOS (not working anymore i guess) and probably even iOS (too lazy, but phonegap should work) that use phonegap.

You'll find these clients under ./clients

Once you start the node.js app you'll have access to a couple of awesome webapps, that are supposed to be used by the DJ to see current crowd activity and musical preferences. Yeah.

I also added my full thesis to the project, with a lot of awesome ideas in it. It might also serve as a good starting point to get to know the framework/apps.

Everything that's implementation related starts at around page 65. :) 

Feel free to contact me for questions, it was my first JS project, so there must be a lot of room for improvement left.

DANCE. DANCE. DANCE.
